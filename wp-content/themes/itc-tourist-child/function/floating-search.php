<div class="floating-search">
	<div class="float-header">
		<h4>Pretraga</h4>

		<div class="contact-close-btn">
			<span class="icon-close"></span>
		</div>
	</div>

	<div class="float-body">
		<div class="float-search-form">
			<form>
				<div class="search-filter-group">
					<select>
						<option>Tip prevoza</option>
						<option>Sopstveni</option>
						<option>Autobuski</option>
						<option>Avio</option>
					</select>
				</div>
				<div class="search-filter-group">
					<select>
						<option>Tip putovanja</option>
						<option>Letovanje</option>
						<option>Zimovanje</option>
						<option>Evropski gradovi</option>
						<option>Nova godina</option>
					</select>
				</div>
				<div class="search-filter-group">
					<select>
						<option>Država</option>
						<option>Grčka</option>
						<option>Bugarska</option>
						<option>Turska</option>
					</select>
				</div>
				<div class="search-filter-group">
					<select>
						<option>Destinacija</option>
						<option>Tasos</option>
						<option>Krf</option>
					</select>
				</div>
				<div class="search-filter-group">
					<select>
						<option>Tip smeštaja</option>
						<option>Apartman</option>
						<option>Hotel</option>
					</select>
				</div>
				<div class="search-filter-group">
					<button class="btn btn-primary">Pretraži <span class="icon-search1"></span></button>
				</div>
			</form>
		</div>
	</div>
</div>