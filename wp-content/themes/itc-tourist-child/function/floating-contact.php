
<div class="floating-contact">
	<div class="float-header">
		<h4>Kontakt</h4>

		<div class="contact-close-btn">
			<span class="icon-close"></span>
		</div>
	</div>

	<div class="float-body">
		<?php if( have_rows('poslovnice', 'options') ): ?>
			<div id="poslovnice-float" class="float-poslovnice">

			    <?php $poslovnica=1; while( have_rows('poslovnice', 'options') ): the_row(); 
			        $grad = get_sub_field('grad');
			        $adresa = get_sub_field('adresa');
			        $email = get_sub_field('contact_email');
			        $telefoni = get_sub_field('telefoni');
			        $mon_fri = get_sub_field('mon_fri');
			        $sat = get_sub_field('sat');
			        $sun = get_sub_field('sun');
			    ?>
			        <div class="single-header-poslovnica">
						<div id="card-<?php echo $poslovnica; ?>">
				        	<button class="btn btn-link" data-toggle="collapse" data-target="#collapse-<?php echo $poslovnica; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $poslovnica; ?>">
				        		<span class="poslovnica-grad"><?php echo $grad; ?></span>
				        	</button>
					    </div>

					    <div id="collapse-<?php echo $poslovnica; ?>" class="collapse <?php if($poslovnica==1) : ?>show<?php endif; ?>" aria-labelledby="card-<?php echo $poslovnica; ?>" data-parent="#poslovnice-float">
				        	<span class="poslovnica-adresa"><?php echo $adresa; ?></span>
				        	<span class="poslovnica-telefoni"><?php echo $telefoni; ?></span>
				        	<span class="poslovnica-email"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></span>
				        	<div>
				        		<h5>Radno vreme:</h5>
				        		<span class="poslovnica-rvreme">
				        			<span class="rvreme-label">Pon-Pet</span> <span class="rvreme-vrednost"><?php echo $mon_fri; ?></span>
				        		</span>
					        	<span class="poslovnica-rvreme">
					        		<span class="rvreme-label">Subota</span> <span class="rvreme-vrednost"><?php echo $sat; ?></span>
					        	</span>
					        	<span class="poslovnica-rvreme">
					        		<span class="rvreme-label">Nedelja</span> <span class="rvreme-vrednost"><?php echo $sun; ?></span>
					        	</span>
					        </div>
					    </div>
			        </div>
			    <?php $poslovnica++; endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
</div>