<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

	<div id="primary" class="content-area mt-0 pt-0">
		<main id="main" class="site-main">
				<?php if ( have_posts() ) : ?>

				<div id="no-header-image">
				    <div class="header-img-mask">
				        <div class="container">
				            <div class="row align-items-end">
				                <div class="col-md-9">
				                    <h1 class="entry-title">
				                    	Tražili ste: 
										<span class="page-description"><?php echo get_search_query(); ?></span>
				                    </h1>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>

				<div class="container mt-4">

                	<div class="row justify-content-center">
                		<div class="col-md-9">
                			<div class="row">
								<?php
								// Start the Loop.
								while ( have_posts() ) : the_post(); ?>

			                        <div class="col-md-6 posebno hover1">
										<?php require('templates/function/prikaz-objekata.php'); ?>
									</div>

								<?php endwhile; ?>

								<?php twentynineteen_the_posts_navigation(); ?>
							</div>
						</div>
					</div>

				</div>

				<?php else :

					get_template_part( 'template-parts/content/content', 'none' );

				endif;
				?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
