<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

<!-- Ako ima header slika -->
<div id="<?php if( get_field('header_image') ) : ?>header-image<?php else : ?>no-header-image<?php endif; ?>">
	<?php if( get_field('header_image') ) : ?>
		<img src="<?php the_field('header_image'); ?>" />
	<?php endif; ?>

		<div class="header-img-mask">
			<div class="container">
				<h1 class="entry-title"><?php single_term_title( '<h1 class="page-title">', '</h1>' ); ?></h1>
			
				<?php if (function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} 
				?>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div id="primary" class="content-area col-md-9">
				<main id="main" class="site-main">
					<?php if ( have_posts() ) : ?>
						<div class="row">
							<?php
							// Start the Loop.
							while ( have_posts() ) :
								the_post(); ?>
								<div class="col-md-6">
									<?php /*
									 * Include the Post-Format-specific template for the content.
									 * If you want to override this in a child theme, then include a file
									 * called content-___.php (where ___ is the Post Format name) and that
									 * will be used instead.
									 */
									get_template_part( 'template-parts/content/content', 'katalozi' ); ?>

								</div>

							<?php endwhile; ?>

						</div>

							<?php twentynineteen_the_posts_navigation();

							// If no content, include the "No posts found" template.
						else :
							get_template_part( 'template-parts/content/content', 'none' );

						endif;
					?>
				</main><!-- #main -->
			</div><!-- #primary -->
			<div id="secondary" class="col-md-3">
				<?php
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 3,
				    'tax_query' => array(
				        array(
				            'taxonomy' => 'category',
				            'field'    => 'id',
				            'terms'    => array(8),
				            'operator' => 'NOT IN',
				        ),
				    ),
				);
				$post_query = new WP_Query($args);

				?>
				<?php if ( $post_query->have_posts() ) : ?>

					<div class="container">
						<div class="novosti-home rounded">

							<div class="novosti-side-title">
								Poslednje objave
							</div>

							<div class="row">
								<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
									<div class="col-md-12">
										<div class="single-blog">
											<a href="<?php the_permalink(); ?>">
												<div class="row">
													<div class="col-md-4">
														<?php the_post_thumbnail( 'vesti-img' ); ?>
													</div>
													<div class="col-md-8">
														<h4><?php the_title(); ?></h4>
													</div>
												</div>
											</a>
										</div>
									</div>
								<?php endwhile; wp_reset_query() ?>
							</div>
						</div>
					</div>

				<?php endif; ?>
			</div>
		</div>
	</div>

<?php
get_footer();
