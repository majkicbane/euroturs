<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-6 error-404 not-found">
						<header class="page-header">
							<h1 class="page-title">Tražena stranica nije pronađena ili se više ne nalazi na ovom linku</h1>
						</header><!-- .page-header -->

						<div class="page-content">
							<p>Pokušajte je pronaći koristeći polje za pretragu ispod:</p>
							<?php get_search_form(); ?>
						</div><!-- .page-content -->
					</div><!-- .error-404 -->
					<div class="col-md-6">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/404.png">
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
