<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

?>

<div class="mob-menu-header">
	<nav id="menu">
		<div class="mob-menu-close close-right-top">
			<span class="icon-close"></span>
		</div>
		<?php wp_nav_menu( array( 'menu' => 'Primary' ) ); ?>
	</nav>
</div>

	<div class="bottom-navigation">
		<div class="row">
			<div class="col-3">
				<a href="<?php echo get_site_url(); ?>">
					<div class="single-menu-item">
						<span class="icon-home"></span>
						<div class="bottom-menu-label">
							Početna
						</div>
					</div>
				</a>
			</div>
			<div class="col-3 open-contact">
				<div class="single-menu-item">
					<span class="icon-call"></span>
					<div class="bottom-menu-label">
						Kontakt
					</div>
				</div>
			</div>
			<div class="col-3 open-search">
				<div class="single-menu-item">
					<span class="icon-search1"></span>
					<div class="bottom-menu-label">
						Pretraga
					</div>
				</div>
			</div>
			<div id="nav-icon" class="col-3">
				<div class="single-menu-item">
					<span class="icon-menu"></span>
					<div class="bottom-menu-label">
						Ponuda
					</div>
				</div>
			</div>
		</div>
	</div>

	</main><!-- #content -->
		
		<div class="newsletter-section">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-5">
						<h3>Prijava za newsletter</h3>
						<p>Prijavite se na našu newsletter listu</p>
					</div>

					<div class="col-md-7">
						<form>
							<div class="newsletter-form-wrapper">
								<input type="email" name="email" placeholder="Email">

								<button type="submit" class="btn btn-newsletter">Prijavite se</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	<footer id="colophon" class="site-footer">

		<div class="footer-widgets">
			<div class="container">
				<div class="row">
					<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
						<div class="col-md-3">
							<?php dynamic_sidebar( 'footer-1' ); ?>
						</div>
					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
						<div class="col-md-3">
							<?php dynamic_sidebar( 'footer-2' ); ?>
						</div>
					<?php endif; ?>
					
					<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
						<div class="col-md-3">
							<?php dynamic_sidebar( 'footer-3' ); ?>
						</div>
					<?php endif; ?>
					
					<?php if(get_field('yuta_logo', 'options') || get_field('footer_logos', 'options')) : ?>
						<div class="col-md-3">
							<div class="footer-logos h-100 d-flex">
								<div class="yuta">
									<img src="<?php the_field('yuta_logo', 'options'); ?>">
								</div>

								<div class="ostali-logoi">
									<?php the_field('footer_logos', 'options'); ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row site-info">
				<?php if(get_field('copyright', 'options')) : ?>
					<div class="col-md-6">
						<div class="display-inline">
							Copyright <?php echo date('Y'); ?> | <?php the_field('copyright', 'options'); ?>
						</div><!-- .site-info -->
					</div>
				<?php endif; ?>

				<?php if(get_field('licenca', 'options')) : ?>
					<div class="col-md-6">
						<div class="text-right">
							<?php the_field('licenca', 'options'); ?>
						</div><!-- .site-info -->
					</div>
				<?php endif; ?>
			</div>
		</div>

	</footer><!-- #colophon -->



	<div class="floating-right">
		<ul>
			<li class="open-search"><span class="icon-search"></span></li>
			<li class="open-contact"><span class="icon-phone"></span></li>
		</ul>
	</div>

	<?php require('function/floating-contact.php'); ?>
	<?php require('function/floating-search.php'); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
