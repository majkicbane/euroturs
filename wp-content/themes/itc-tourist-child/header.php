<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class="overlay-black"></div>

<?php wp_body_open(); ?>
<div id="page" class="site">

	<header id="masthead" class="site-header">
		<div class="container">

			<div class="header-top d-flex justify-content-between ">
				<?php if ( get_field('logo', 'options') ) : ?>
					<div class="site-logo"><a href="<?php echo get_site_url(); ?>"><img src="<?php the_field('logo', 'options') ?>"></a></div>
				<?php endif; ?>

				<?php if( have_rows('poslovnice', 'options') ): ?>
					<div class="header-poslovnice">
					    <?php while( have_rows('poslovnice', 'options') ): the_row(); 
					        $grad = get_sub_field('grad');
					        $adresa = get_sub_field('adresa');
					        $email = get_sub_field('contact_email');
					        $telefoni = get_sub_field('telefoni');
					        $mon_fri = get_sub_field('mon_fri');
					        $sat = get_sub_field('sat');
					        $sun = get_sub_field('sun');
					    ?>
					        <div class="single-header-poslovnica">
					        	<span class="poslovnica-grad"><?php echo $grad; ?></span>
					        	<span class="poslovnica-telefoni"><?php echo $telefoni; ?></span>
					        </div>
					    <?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>

			<div class="header-bottom">
				<?php ubermenu( 'main' ); ?>
			</div>

		</div><!-- .site-branding-container -->
	</header><!-- #masthead -->

	<div class="floating-social">
		<ul>
			<li><span class="icon-facebook"></span></li>
			<li><span class="icon-twitter"></span></li>
			<li><span class="icon-youtube"></span></li>
			<li><span class="icon-instagram"></span></li>
		</ul>
	</div>

	<main id="content" class="site-content">
