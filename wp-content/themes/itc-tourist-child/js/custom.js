    // function initMap() {
    //  // The location of Uluru
    //       const uluru = { lat: 40.016661, lng: 23.5232155 };
    //       // The map, centered at Uluru
    //       const map = new google.maps.Map(document.getElementById("map"), {
    //         zoom: 4,
    //         center: uluru,
    //       });
    //       // The marker, positioned at Uluru
    //       const marker = new google.maps.Marker({
    //         position: uluru,
    //         map: map,
    //       });
    // }

jQuery( document ).ready(function($) {

      // Add smooth scrolling to all links
      $("a.smooth-scroll").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();

          // Store hash
          var hash = this.hash;

          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top - 130
          }, 800, function(){

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if
      });
    

    // Create the script tag, set the appropriate attributes
    var script = document.createElement('script');
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBox06KdtIt6AiO-C3NYqZhsFNtDSawAPw';
    script.defer = true;
    // Attach your callback function to the `window` object

    // Append the 'script' element to 'head'
    document.head.appendChild(script);




    /*mobile menu*/
    var menu = new MmenuLight(
        document.querySelector('#menu'),
        'all'
    );

    var navigator = menu.navigation({
        // selectedClass: 'Selected',
        // slidingSubmenus: true,
        theme: 'light',
        title: 'Ponuda'
    });

    var drawer = menu.offcanvas({
        position: 'left'
    });

    //  Open the menu.
    document.querySelector('#nav-icon')
    .addEventListener('click', evnt => {
        evnt.preventDefault();
        drawer.open();
    });

    jQuery(".mob-menu-close").click(function() {
         drawer.close();
    });


    
    $('.open-contact').click(function() {
    	$('.floating-contact').toggleClass('open');
        $('.floating-search').removeClass('open');
        $('.floating-filteri-ponuda').removeClass('open');
        $('.overlay-black').toggleClass('open');
    });

    $('.floating-contact .contact-close-btn').click(function() {
    	$('.floating-contact').removeClass('open');
        $('.overlay-black').removeClass('open');
    });

    $('.open-search').click(function() {
        $('.floating-search').toggleClass('open');
        $('.floating-contact').removeClass('open');
        $('.floating-filteri-ponuda').removeClass('open');
        $('.overlay-black').toggleClass('open');
    });

    $('.floating-search .contact-close-btn').click(function() {
        $('.floating-search').removeClass('open');
        $('.overlay-black').removeClass('open');
    });

    $('#nav-icon').click(function() {
        $('.floating-search').removeClass('open');
        $('.floating-contact').removeClass('open');
        $('.floating-filteri-ponuda').removeClass('open');
    });

    $('.btn-filter').click(function() {
        $('.floating-search').removeClass('open');
        $('.floating-contact').removeClass('open');
        $('.floating-filteri-ponuda').addClass('open');
        $('.overlay-black').toggleClass('open');
    });

    $('.floating-filteri-ponuda .contact-close-btn').click(function() {
        $('.floating-filteri-ponuda').removeClass('open');
        $('.overlay-black').removeClass('open');
    });


    $('.overlay-black').click(function() {
        $('.overlay-black').removeClass('open');
        $('.floating-search').removeClass('open');
        $('.floating-contact').removeClass('open');
        $('.floating-filteri-ponuda').removeClass('open');
    })

    $('.read-more-opis').click(function() {
    	$('.add-overlay').toggleClass('light-overlay');
    })


    jQuery('.tab-slider').owlCarousel({
        loop: true,
        nav: true,
        dots: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        dotsContainer: '#customDots',
        navText: ["<span class='icon-arrow-left'></span>", "<span class='icon-arrow-right'></span>"],
        margin: 30,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });

    jQuery('.onama-slider').owlCarousel({
        loop: false,
        nav: false,
        dots: false,
        autoplay: false,
        autoplayTimeout: 3500,
        autoplayHoverPause: true,
        margin: 30,
        responsive: {
            0: {
                items: 1,
                stagePadding: 50,
                autoplay: true,
                loop: true,
            },
            600: {
                items: 2
            },
            1200: {
                items: 4,
                mouseDrag: false,
            }
        }
    });

    jQuery('.soba-slider').owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        autoplay: true,
        autoplayTimeout: 3500,
        autoplayHoverPause: true,
        margin: 30,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });



    jQuery(document).ready(function(){
        jQuery('.euroturs-galerija').lightGallery({
            selector: '.gallery-item'
        });

        jQuery('#video-gallery').lightGallery({
            youtubePlayerParams: {
                loadYoutubeThumbnail: true,
                youtubeThumbSize: 'default',
                modestbranding: 1,
                showinfo: 0,
                rel: 0
            }
        }); 
    });

    jQuery('div.otvori-galeriju').click(function(){
        jQuery('#euroturs-galerija li').first().click();
    });

    jQuery('div.otvori-video').click(function(){
        jQuery('#video-gallery a').first().click();
    });
});


