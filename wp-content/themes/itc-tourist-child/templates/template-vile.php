<?php
/**
 * Template Name: 2. Aranzmani
 *
 * @package TA Pluton
 */

get_header();
global $post;
$currentId = $post->ID;
$tipSmestaja = get_post_meta($currentId, 'accommodation_type', true);
$kategorija = get_post_meta($currentId, 'kategorija', true);
$sadrzaji = get_post_meta($currentId, 'features', true);
$sobe = get_post_meta($currentId, 'rooms', true);
?>

<div class="page-header">
	<div class="container">
		<div class="page-title <?php if($tipSmestaja != 'Hotel') : ?>pt-5<?php endif; ?>">

			<?php if($tipSmestaja == 'Hotel' && $kategorija) : ?>
				<div class="hotel-kategorija">
					<?php require('function/hotel-stars.php'); ?>
				</div>
			<?php endif; ?>

			<h1><?php the_title(); ?></h1>
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
				  yoast_breadcrumb( '</p><p id="breadcrumbs">','</p><p>' );
				}
			?>
		</div>
	</div>
</div>

	<div class="container">
		<main id="main" class="site-main smestaj-ponuda" role="main">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				$images = get_field('galerija');
				if(wp_is_mobile()) {
					$mobGallery = 4;
				} else {
					$mobGallery = 3;
				}
				?>
				<?php if( $images ): ?>

					<div class="objekat-galerija mt-5 row euroturs-galerija">

					    <div class="galerija-pozicija-1 col-8">
					        <?php $slika=1; foreach( $images as $image ): ?>
					            <div class="gallery-item <?php if($slika == 1) : ?> prva <?php elseif($slika>1 && $slika <= $mobGallery) : ?> druga-treca <?php else : ?> ostale <?php endif; ?>"  data-src="<?php echo wp_get_attachment_url( $image ); ?>">
	                				<a href="#">
					                     <?php echo wp_get_attachment_image( $image, 'full' ); ?>

					                     <?php if($mobGallery == 4 && $slika==4) : ?>
					                     	<?php echo '<div class="gal-number">+' . (count( $images )-4) . '</div>';  ?>
					                     <?php endif; ?>
					                </a>
					            </div>

					            <?php if($slika==1) : ?>
						            </div> <!-- zatvorena pozicija 1 -->

						            <div class="galerija-pozicije-2-3 d-flex flex-wrap col-4">
						        <?php endif; ?>

					        	<?php if($slika==$mobGallery) : ?>
							        </div> <!-- zatvorena pozicija 2 i 3 -->

							        <div class="galerija-pozicije-ostale d-flex flex-wrap col-12">
							    <?php endif; ?>

					        <?php $slika++; endforeach; ?>
					    </div>
					</div>
				<?php endif; ?>

				<div class="smestaj-tabovi">

					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="opis-tab" data-toggle="tab" href="#opis" role="tab" aria-controls="opis" aria-selected="true">Opis</a>
						</li>
						<li class="nav-item">
							<a class="nav-link smooth-scroll" id="sobe-tab" href="#smestaj-sobe">Sobe</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="cenovnik-tab" data-toggle="tab" href="#cenovnik" role="tab" aria-controls="cenovnik" aria-selected="false">Cenovnik</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="prevoz-tab" data-toggle="tab" href="#prevoz" role="tab" aria-controls="prevoz" aria-selected="false">Prevoz</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="komentari-tab" data-toggle="tab" href="#komentari" role="tab" aria-controls="komentari" aria-selected="false">Komentari <span class="comment-bubble"><?php echo get_comments_number(); ?></span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="napomene-tab" data-toggle="tab" href="#napomene" role="tab" aria-controls="napomene" aria-selected="false">Napomene</a>
						</li>
					</ul>

				</div>

				<div class="smestaj-tabovi-content row">
					<div class="col-md-8">

						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="opis" role="tabpanel" aria-labelledby="opis-tab">
								<?php the_content(); ?>

								
								<?php if( $sadrzaji ) : ?>
									<div class="smestaj-sadrzaj">
										<h4>Smeštaj</h4>

										<ul>
										    <?php foreach( $sadrzaji as $sadrzaj ): ?>
										        <li><?php echo $sadrzaj; ?></li>
										    <?php endforeach; ?>
										</ul>
									</div>
								<?php endif; ?>

							</div>
							<div class="tab-pane fade" id="cenovnik" role="tabpanel" aria-labelledby="cenovnik-tab">
								
								<div class="responsive-table">
									<?php echo do_shortcode("[accomodation-rates hotel=$currentId]"); ?>
								</div>

							</div>
							<div class="tab-pane fade" id="prevoz" role="tabpanel" aria-labelledby="prevoz-tab">...</div>

							<div class="tab-pane fade" id="komentari" role="tabpanel" aria-labelledby="komentari-tab">
								<?php if ( comments_open() || get_comments_number() ) {
									comments_template();
								} ?>
							</div>

							<div class="tab-pane fade" id="napomene" role="tabpanel" aria-labelledby="napomene-tab">...</div>
						</div>

					</div>

					<div class="col-md-4">


						<?php if(get_field('latitude') || get_field('longitude') || get_field('airport_distance') || get_field('center_distance') || get_field('bus_distance') || get_field('beach_distance') || get_field('skibus_distance') || get_field('gondola_distance')) : ?>

							<div class="alt-text-onama lokacija-box rounded">
								<h4>Lokacija</h4>

								<?php if(get_field('address')) : ?>
									<div class="lokacija-adresa">
										<span>Adresa:</span>
										<div><?php the_field('address'); ?></div>
									</div>
								<?php endif; ?>

								<div class="polozaj-objekta">
									<?php if(get_field('airport_distance')) : ?>
										<div class="single-polozaj">
											<div class="polozaj-label">Udaljenost od aerodroma</div>
											<div class="polozaj-vrednost">
												<?php $airport = get_field('airport_distance');

												if($airport>=1000) : ?>
													<?php echo ($airport / 1000); ?> km
												<?php else : ?>
													<?php echo $airport; ?> m
												<?php endif; ?>
											</div>
										</div>
									<?php endif; ?>
									<?php if(get_field('center_distance')) : ?>
										<div class="single-polozaj">
											<div class="polozaj-label">Udaljenost od centra</div>
											<div class="polozaj-vrednost">
												<?php $center = get_field('center_distance');

												if($center>=1000) : ?>
													<?php echo ($center / 1000); ?> km
												<?php else : ?>
													<?php echo $center; ?> m
												<?php endif; ?>
											</div>
										</div>
									<?php endif; ?>
									<?php if(get_field('bus_distance')) : ?>
										<div class="single-polozaj">
											<div class="polozaj-label">Udaljenost od linije gradskog prevoza</div>
											<div class="polozaj-vrednost">
												<?php $bus = get_field('bus_distance');

												if($bus>=1000) : ?>
													<?php echo ($bus / 1000); ?> km
												<?php else : ?>
													<?php echo $bus; ?> m
												<?php endif; ?>
											</div>
										</div>
									<?php endif; ?>
									<?php if(get_field('beach_distance')) : ?>
										<div class="single-polozaj">
											<div class="polozaj-label">Udaljenost od plaže</div>
											<div class="polozaj-vrednost">
												<?php $beach = get_field('beach_distance');

												if($beach>=1000) : ?>
													<?php echo ($beach / 1000); ?> km
												<?php else : ?>
													<?php echo $beach; ?> m
												<?php endif; ?>
											</div>
										</div>
									<?php endif; ?>
									<?php if(get_field('skibus_distance')) : ?>
										<div class="single-polozaj">
											<div class="polozaj-label">Udaljenost od ski busa</div>
											<div class="polozaj-vrednost">
												<?php $skibus = get_field('skibus_distance');

												if($skibus>=1000) : ?>
													<?php echo ($skibus / 1000); ?> km
												<?php else : ?>
													<?php echo $skibus; ?> m
												<?php endif; ?>
											</div>
										</div>
									<?php endif; ?>
									<?php if(get_field('gondola_distance')) : ?>
										<div class="single-polozaj">
											<div class="polozaj-label">Udaljenost od staze/gondole</div>
											<div class="polozaj-vrednost">
												<?php $gondola = get_field('gondola_distance');

												if($gondola>=1000) : ?>
													<?php echo ($gondola / 1000); ?> km
												<?php else : ?>
													<?php echo $gondola; ?> m
												<?php endif; ?>
											</div>
										</div>
									<?php endif; ?>
								</div>

								<?php if(get_field('latitude') || get_field('longitude')) : ?>
									<a href="#" data-toggle="modal" data-target="#smestaj-mapa">
										<div class="mapa-lokacija">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mapa.png">
											<div class="map-label">
												Pogledaj mapu
											</div>
										</div>
									</a>
								<?php endif; ?>

							</div>
						<?php endif; ?>

						<div class="pdf-ponuda">
							<a href="<?php echo get_site_url() . '/filedownload?id=' . get_the_ID(); ?>" target="_blank">
								<button class="btn btn-primary btn-download"><span class="icon-download-button"></span> Preuzmi PDF</button>
							</a>
						</div>
					</div>
				</div>

				<?php if($sobe) : ?>
					<div id="smestaj-sobe">
						<h3>Sobe</h3>

						<?php if(get_field('rooms_text')) : ?>
							<div>
								<?php the_field('rooms_text'); ?>
							</div>
						<?php endif; ?>


						<div class="sobe-wrapper">

							<?php for($i=0;$i<$sobe;$i++) : 

					    		$sobaTitle_key = 'rooms_'.$i.'_room_name';
						        $sobaTitle = get_post_meta($currentId, $sobaTitle_key, true);

					    		$sobaDesc_key = 'rooms_'.$i.'_room_description';
						        $sobaDesc = get_post_meta($currentId, $sobaDesc_key, true);

						        $features_key = 'rooms_'.$i.'_room_features';
						        $features = get_post_meta($currentId, $features_key, true);

						        $gallery_key =  'rooms_'.$i.'_room_gallery';
						        $gallery = get_post_meta($currentId, $gallery_key, true);

						        $minAdl_key = 'rooms_'.$i.'_min_adults';
						        $minAdl = get_post_meta($currentId, $minAdl_key, true);

						        $minPersons_key = 'rooms_'.$i.'_min_persons';
						        $minPersons = get_post_meta($currentId, $minPersons_key, true);

						        $maxPersons_key = 'rooms_'.$i.'_max_persons';
						        $maxPersons = get_post_meta($currentId, $maxPersons_key, true);

						        $capacity_key = 'rooms_'.$i.'_basic_capacity';
						        $capacity = get_post_meta($currentId, $capacity_key, true);

						        $extraBed_key = 'rooms_'.$i.'_additional_capacity_beds';
						        $extraBed = get_post_meta($currentId, $extraBed_key, true);

						        $extraKids_key = 'rooms_'.$i.'_additional_capacity_kids';
						        $extraKids = get_post_meta($currentId, $extraKids_key, true);
						    ?>
								<div class="soba-card rounded">
									<?php require('function/smestaj-soba.php'); ?>
								</div>
							<?php endfor; ?>
						</div>
					</div>
				<?php endif; ?>


			<?php endwhile; // end of the loop. ?>
		</main><!-- #main -->
	</div>



<?php if(get_field('latitude') || get_field('longitude')) : ?>

	<!-- Modal -->
	<div class="modal fade" id="smestaj-mapa" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle"><?php the_title(); ?> - Lokacija na mapi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span class="icon-close"></span>
					</button>
				</div>
				<div class="modal-body">

					<div id="map"></div>

				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		
	    jQuery('#smestaj-mapa').on('shown.bs.modal', function () {
	        let lat = '<?php the_field("latitude"); ?>';
	        let long = '<?php the_field("longitude"); ?>';
	        let eticon = '<?php echo get_stylesheet_directory_uri(); ?>/img/pin.png';

	        console.log(lat, long);

	        if(jQuery(this).find('#map').is(':empty')) {
	            // The location of Uluru
	          const loc = { lat: parseFloat(lat), lng: parseFloat(long) };
	          // The map, centered at Uluru
	          const map = new google.maps.Map(document.getElementById("map"), {
	            zoom: 15,
	            center: loc,
	          });
	          // The marker, positioned at Uluru
	          const marker = new google.maps.Marker({
	            position: loc,
    			icon: eticon,
	            map: map,
	          });
	        }
	         
	    });

	</script>

<?php endif; ?>

<?php get_footer(); ?>