<?php
/**
 * Template Name: 1. dubina - Sezona
 *
 * @package TA Pluton
 */
get_header(); ?>
<!-- Ako ima header slika -->

<?php 
global $post;
$currentID = $post->ID;
$pages_no = get_pages("child_of=$currentID&depth=1&parent=$currentID");
$count = count($pages_no);
if( get_field('header_image') ) { ?>
<div id="header-image">
	<img src="<?php the_field('header_image'); ?>" />
	<div class="header-img-mask"></div>
	<div class="destinacija-title">
		<div class="container">
			<header class="entry-header">
				<div class="row">
					<div class="col-md-12">
					<?php 
					if($count == 1) {
					the_title( '<h1 class="entry-title">', '</h1> <span class="br-destinacija">('. $count . ' država u ponudi)</span>' );
					}
					else if($count > 1 && $count < 5) {
					the_title( '<h1 class="entry-title">', '</h1> <span class="br-destinacija">('. $count . ' države u ponudi)</span>' );
					}
					else {
					the_title( '<h1 class="entry-title">', '</h1> <span class="br-destinacija">('. $count . ' država u ponudi)</span>' );
					} ?>
					<?php if (function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						} 
					?>
					</div>
				</div>
			</header><!-- .entry-header -->
		</div>
	</div>
</div>
<div class="container ima-header-slika">
<?php } else { ?>
<div class="container nema-header-slika">
<?php } ?>
		<div class="row">
			<div id="primary" class="col-md-12">
				<main id="main" class="site-main" role="main">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', 'drzave' ); ?>
					<?php endwhile; // end of the loop. ?>
				</main><!-- #main -->

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php $parent = $post->ID; ?>
<?php endwhile; endif; ?>
<?php
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$dest=(array(
	'post_type'=>'page',
	'orderby'=>'menu_order',
	'order'=>'ASC',
	'post_parent'=>$parent,
	'posts_per_page' => 12,
	'paged' => $paged,
	));
$dest = new WP_Query($dest);
?>
<?php if($dest->have_posts()) : ?>
<div class="row destinacije destinacije-listing">
	<?php while ($dest->have_posts()) : $dest->the_post(); ?>
		<div class="col-md-3 posebno hover12">    	
	    	<div class="single-destinacija-drzave">
	        		<figure>
		        		<a href="<?php the_permalink() ?>">
			        		<?php the_post_thumbnail( 'destinacije' ); ?>
			        	</a>
			        </figure>
				<h3>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h3>
			</div>
		</div>
	<?php endwhile; wp_reset_postdata(); ?>
			<?php if ($dest->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
			  <nav class="prev-next-posts">
			    <div class="prev-posts-link">
			      <?php echo get_next_posts_link( 'Older Entries', $dest->max_num_pages ); // display older posts link ?>
			    </div>
			    <div class="next-posts-link">
			      <?php echo get_previous_posts_link( 'Newer Entries' ); // display newer posts link ?>
			    </div>
			  </nav>
			<?php } ?>
	</div>
<?php endif; ?>
			</div><!-- #primary -->
	</div>
</div>
<?php get_footer(); ?>