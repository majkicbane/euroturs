<?php
/**
 * Template Name: Avio karte
 *
 * @package Euroturs
 */

get_header();
?>

<!-- Ako ima header slika -->
<div id="<?php if( get_field('header_image') ) : ?>header-image<?php else : ?>no-header-image<?php endif; ?>">
	<?php if( get_field('header_image') ) : ?>
		<img src="<?php the_field('header_image'); ?>" />
	<?php endif; ?>

		<div class="header-img-mask">
			<div class="container">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			
				<?php if (function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} 
				?>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="container">
					<div class="row">
						<?php if(get_field('alt_tekst')) : ?>
							<div class="col-md-8">
						<?php else : ?>
							<div class="col-md-12">
						<?php endif; ?>

								<div class="entry-content">
									<?php
									the_content();

									wp_link_pages(
										array(
											'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
											'after'  => '</div>',
										)
									);
									?>
								</div><!-- .entry-content -->

							</div>

						<?php if(get_field('alt_tekst')) : ?>
							<div class="col-md-4">
								<div class="entry-content">
									<div class="alt-text-onama rounded">
										<?php the_field('alt_tekst'); ?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>


				<?php
				$args = array(
					'post_type' => 'aviokarte',
					'posts_per_page' => -1
				);
				$post_query = new WP_Query($args);

				?>
				<?php if ( $post_query->have_posts() ) : ?>

					<div class="container">
						<div class="entry-content">

							<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>

								<div class="single-aviokarta mb-3">
									<div class="row align-items-center h-100">
										<div class="col-md-1 col-3 h-100">
											<div class="avio-logo">
												<?php
													$prevoznik = get_field('prevoznik');
													echo wp_get_attachment_image( $prevoznik, 'full' );
												?>
											</div>
										</div>
										<div class="col-md-3 col-9">
											<div class="polazak-od-do d-flex">
												<?php 
													$fieldOd = get_field_object('polazak');
													$valueOd = get_field('polazak');
													$labelOd = $fieldOd['choices'][$valueOd];

													$fieldDo = get_field_object('dolazak');
													$valueDo = get_field('dolazak');
													$labelDo = $fieldDo['choices'][$valueDo];
												?>
												<div class="polazak-od">
													<?php echo $labelOd; ?>
												</div>
												<div class="polazak-icon">
													<span class="icon-arrow"></span>
												</div>
												<div class="polazak-do">
													<?php echo $labelDo; ?>
												</div>
											</div>
											<?php if(get_field('datum')) : ?>
												<div class="polazak-datum">
													<span class="icon-date"></span> <?php the_field('datum'); ?>
												</div>
											<?php endif; ?>
										</div>
										<div class="col-md-8 mob-hidden">
											<?php if(in_array("Da", get_field('obnavljanje') )) : ?>
												<div class="avio-note">
													Čeka sa obnavljanje letova
												</div>
											<?php else : ?>
												<div class="row align-items-center">
													<div class="col-md-2">
														<div class="avio-label">Dani</div>

														<?php
														$dani = get_field('dani');
														if( $dani ): ?>
														    <?php $d=1; foreach( $dani as $dan ): ?>
														        <?php if($d != 1) : ?> /<?php endif; ?> <?php echo $dan; ?>
														    <?php $d++; endforeach; ?>
														<?php endif; ?>
													</div>
													<div class="col-md-4">
														<?php if( have_rows('satnica_polazak') ): ?>
															<div class="row polazak-wrapper">
																<div class="col-4">
																	<div class="polazak-dolazak"><?php the_field('polazak'); ?> - <?php the_field('dolazak'); ?></div>
																</div>
																<div class="col-8">
																	<?php while( have_rows('satnica_polazak') ): the_row(); ?>
																		<div class="satnica">
																			<div class="gold-label">
																				<span class="icon-clock"></span>
																				<?php if(get_sub_field('dan')) : ?>
																					<span class="dan-label"><?php the_sub_field('dan'); ?></span>
																				<?php endif; ?>
																			</div>
																			<div>
																				<?php the_sub_field('vreme'); ?>
																			</div>
																		</div>
																	<?php endwhile; ?>
																</div>
															</div>
														<?php endif; ?>

														<?php if( have_rows('satnica_dolazak') ): ?>
															<div class="row dolazak-wrapper">
																<div class="col-4">
																	<div class="polazak-dolazak"><?php the_field('dolazak'); ?> - <?php the_field('polazak'); ?></div>
																</div>
																<div class="col-8">
																	<?php while( have_rows('satnica_dolazak') ): the_row(); ?>
																		<div class="satnica">
																			<div class="gold-label">
																				<span class="icon-clock"></span>
																				<?php if(get_sub_field('dan')) : ?>
																					<span class="dan-label"><?php the_sub_field('dan'); ?></span>
																				<?php endif; ?>
																			</div>
																			<div>
																				<?php the_sub_field('vreme'); ?>
																			</div>
																		</div>
																	<?php endwhile; ?>
																</div>
															</div>
														<?php endif; ?>
													</div>
													<div class="col-md-2">
														<div class="avio-label">Cena OW</div>
														<?php the_field('cena_ow'); ?>
													</div>
													<div class="col-md-2">
														<div class="avio-label">Cena RT</div>
														<?php the_field('cena_rt'); ?>
													</div>
													<div class="col-md-2">
														<div class="avio-label">Prtljag</div>
														<?php the_field('prtljag'); ?>
													</div>
												</div>
											<?php endif; ?>
										</div>
									</div>
								</div>

							<?php endwhile; wp_reset_query() ?>
						</div>
					</div>

				<?php endif; ?>

							<?php if ( get_edit_post_link() ) : ?>
								<footer class="entry-footer">
									<?php
									edit_post_link(
										sprintf(
											wp_kses(
												/* translators: %s: Post title. Only visible to screen readers. */
												__( 'Edit <span class="screen-reader-text">%s</span>', 'twentynineteen' ),
												array(
													'span' => array(
														'class' => array(),
													),
												)
											),
											get_the_title()
										),
										'<span class="edit-link">' . twentynineteen_get_icon_svg( 'edit', 16 ),
										'</span>'
									);
									?>
								</footer><!-- .entry-footer -->
							<?php endif; ?>

			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
