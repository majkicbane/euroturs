<?php
/**
 * Template Name: Sidebar
 *
 * @package Euroturs
 */

get_header();
?>

<!-- Ako ima header slika -->
<div id="<?php if( get_field('header_image') ) : ?>header-image<?php else : ?>no-header-image<?php endif; ?>">
	<?php if( get_field('header_image') ) : ?>
		<img src="<?php the_field('header_image'); ?>" />
	<?php endif; ?>

		<div class="header-img-mask">
			<div class="container">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			
				<?php if (function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} 
				?>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="entry-content">
								<?php
								the_content();

								wp_link_pages(
									array(
										'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
										'after'  => '</div>',
									)
								);
								?>
							</div><!-- .entry-content -->
						</div>

						<div id="sidebar" class="col-md-3">
							<?php
							$args = array(
								'post_type' => 'post',
								'posts_per_page' => 3,
							    'tax_query' => array(
							        array(
							            'taxonomy' => 'category',
							            'field'    => 'id',
							            'terms'    => array(8)
							        ),
							    ),
							);
							$post_query = new WP_Query($args);

							?>
							<?php if ( $post_query->have_posts() ) : ?>

								<div class="container">
									<div class="novosti-home rounded">

										<div class="novosti-side-title">
											Katalozi
										</div>

										<div class="row">
											<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
												<div class="col-md-12">
													<div class="single-blog">
														<a href="<?php the_permalink(); ?>">
															<div class="row">
																<div class="col-md-2 col-1">
																	<span class="icon-pdf-file"></span>
																</div>
																<div class="col-md-10 col-11">
																	<h4><?php the_title(); ?></h4>
																</div>
															</div>
														</a>
													</div>
												</div>
											<?php endwhile; wp_reset_query() ?>
										</div>
									</div>
								</div>

							<?php endif; ?>

							
							<?php
							$args = array(
								'post_type' => 'post',
								'posts_per_page' => 3,
							    'tax_query' => array(
							        array(
							            'taxonomy' => 'category',
							            'field'    => 'id',
							            'terms'    => array(8),
							            'operator' => 'NOT IN',
							        ),
							    ),
							);
							$post_query = new WP_Query($args);

							?>
							<?php if ( $post_query->have_posts() ) : ?>

								<div class="container">
									<div class="novosti-home rounded">

										<div class="novosti-side-title">
											Poslednje objave
										</div>

										<div class="row">
											<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
												<div class="col-md-12">
													<div class="single-blog">
														<a href="<?php the_permalink(); ?>">
															<div class="row">
																<div class="col-md-4">
																	<?php the_post_thumbnail( 'vesti-img' ); ?>
																</div>
																<div class="col-md-8">
																	<h4><?php the_title(); ?></h4>
																</div>
															</div>
														</a>
													</div>
												</div>
											<?php endwhile; wp_reset_query() ?>
										</div>
									</div>
								</div>

							<?php endif; ?>
						</div>
					</div>
				</div>

				<?php if ( get_edit_post_link() ) : ?>
					<footer class="entry-footer">
						<?php
						edit_post_link(
							sprintf(
								wp_kses(
									/* translators: %s: Post title. Only visible to screen readers. */
									__( 'Edit <span class="screen-reader-text">%s</span>', 'twentynineteen' ),
									array(
										'span' => array(
											'class' => array(),
										),
									)
								),
								get_the_title()
							),
							'<span class="edit-link">' . twentynineteen_get_icon_svg( 'edit', 16 ),
							'</span>'
						);
						?>
					</footer><!-- .entry-footer -->
				<?php endif; ?>

			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
