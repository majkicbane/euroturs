<?php
/**
 * Template Name: Nagrade
 *
 * @package Euroturs
 */

get_header();
?>

<!-- Ako ima header slika -->

<div id="<?php if( get_field('header_image') ) : ?>header-image<?php else : ?>no-header-image<?php endif; ?>">
	<?php if( get_field('header_image') ) : ?>
		<img src="<?php the_field('header_image'); ?>" />
	<?php endif; ?>

		<div class="header-img-mask">
			<div class="container">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			
				<?php if (function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} 
				?>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="container">

					<?php if( have_rows('nagrade') ): ?>
					    <div id="euroturs-galerija" class="nagrade-galerija euroturs-galerija row">
					    <?php while( have_rows('nagrade') ): the_row(); 
					        $image = get_sub_field('image');
					        $border = get_sub_field('border');
					        $opis = get_sub_field('nagrada');
					    ?>
					        <div class="col-6 col-md-3 gallery-item" data-src="<?php echo esc_url($image['url']); ?>" data-sub-html="<?php echo $opis; ?>">
			                    <a href="" class="nagrade-ram okvir-<?php echo $border; ?>">
			                        <img class="img-responsive" src="<?php echo esc_url($image['sizes']['ponude-img']); ?>">
			                    </a>
					        </div>
					    <?php endwhile; ?>
					    </div>
					<?php endif; ?>

				</div>

			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
