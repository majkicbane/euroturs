<?php
/**
 * Template Name: 1. Prikaz ponuda
 *
 * @package TA Pluton
 */
global $post;
$currentID = $post->ID;
$pages_no = get_pages("child_of=$currentID&depth=1&parent=$currentID");
$count = count($pages_no);
$images = get_field('galerija');

get_header(); ?>


<!-- Ako ima header slika -->
<div id="<?php if( get_field('header_image') ) : ?>header-image<?php else : ?>no-header-image<?php endif; ?>">
    <?php if( get_field('header_image') ) : ?>
        <img src="<?php the_field('header_image'); ?>" />
    <?php endif; ?>

    <div class="header-img-mask">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-md-9">
                    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                    <?php if (function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                        } 
                    ?>
                </div>
                <?php if(!wp_is_mobile()) : ?>
                    <div class="col-md-3">
                        <div class="header-links">
                            <?php if( $images ): ?>
                                <?php $slika=1; foreach( $images as $image ): ?>
                                    <?php if($slika==1) : ?>
                                        <div class="otvori-galeriju">
                                            <div class="header-links-padding">
                                                <span class="icon-large icon-picture"></span> <span>Pogledajte galeriju fotografija</span>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php $slika++; endforeach; ?>
                            <?php endif; ?>

                            <?php if(have_rows('yt_videos')) : ?>
                                <div class="otvori-video">
                                    <div class="header-links-padding">
                                        <span class="icon-large icon-video"></span> <span>Pogledajte video</span>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>


        </div>
    </div>
</div>


<div class="main-content">
    <div class="container">

    <?php if(wp_is_mobile() && ($images || have_rows('yt_videos') || get_field('mapa') )) : ?>
        <div class="mobile-buttons-header">
            <div class="header-links-mob row">

                <?php if( $images ): ?>
                    <?php $slika=1; foreach( $images as $image ): ?>
                        <?php if($slika==1) : ?>
                            <div class="col-4 otvori-galeriju">
                                <div class="header-links-padding rounded">
                                    <span class="icon-large icon-picture"></span> <span>Foto</span>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php $slika++; endforeach; ?>
                <?php endif; ?>

                <?php if(have_rows('yt_videos')) : ?>
                    <div class="col-4 otvori-video">
                        <div class="header-links-padding rounded">
                            <span class="icon-large icon-video"></span> <span>Video</span>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if(get_field('mapa')) : ?>
                    <div class="col-4 otvori-mapu">
                        <div class="header-links-padding rounded">
                            <span class="icon-large icon-map"></span> <span>Mapa</span>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>

        <div class="site-content destinacija-opis" role="main">
            <?php if(get_the_content() || get_field('mapa') || get_field('kratki_opis')) : ?>
                <div class="row">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="<?php if(get_field('mapa')) : ?>col-md-9<?php else : ?>col-md-12<?php endif; ?>">
                            <div class="drzava-short-conent">
                                <?php the_field('kratki_opis'); ?>

                                <?php if(get_the_content()) : ?>
                                    <div class="collapse" id="citaj-sve">
                                        <?php the_content(); ?>
                                    </div>

                                    <div class="add-overlay light-overlay">
                                        <a class="read-more-opis" data-toggle="collapse" href="#citaj-sve" role="button" aria-expanded="false" aria-controls="collapseExample">Čitaj opširnije</a>
                                    </div>
                                <?php endif; ?>
                            </div>

                        </div>

                    <?php endwhile; // end of the loop. ?>

                    <?php if(get_field('mapa')) : ?>
                        <div class="col-md-3">
                            <div class="mapa-lokacija">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mapa.png">
                                <div class="map-label">
                                    Pogledaj mapu
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

        </div><!-- #main -->


    <?php /*if(in_array( 'Da', get_field('iframe_ponude')) && get_field('iframe_url')) : ?>

        <div class="iframe-ponuda">
            <?php the_field('iframe_url'); ?>
        </div>

    <?php else : */?>


        <?php if(wp_is_mobile()) : ?>
            <div class="btn-for-filter">
                <button class="btn btn-filter"><span class="icon-filter"></span> Filteri</button>
            </div>
        <?php endif; ?>

        <div class="row">
            <aside class="col-md-3 <?php if(wp_is_mobile()) : ?>floating-filteri-ponuda<?php endif; ?>">

                <?php if(wp_is_mobile()) : ?>
                    <div class="float-header">
                        <h4>Filteri</h4>

                        <div class="contact-close-btn">
                            <span class="icon-close"></span>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="drzava-filteri">

                    <div class="single-filter-wrapper">
                        <h6>Tip smestaja:</h6>
                        <label class="filter-label">Hotel
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>

                        <label class="filter-label">Apartman
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <div class="single-filter-wrapper filter-destinacija">
                        <h6>Destinacija:</h6>
                        <label class="filter-label">Asprovalta
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>

                        <label class="filter-label">Pefkohori
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="filter-label">Krf
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>

                        <label class="filter-label">Paralija
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="filter-label">Krit
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>

                        <label class="filter-label">Tasos
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="filter-label">Asprovalta
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>

                        <label class="filter-label">Pefkohori
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="filter-label">Krf
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>

                        <label class="filter-label">Paralija
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="filter-label">Krit
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>

                        <label class="filter-label">Tasos
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <div class="single-filter-wrapper">
                        <h6>Tip prevoza:</h6>
                        <label class="filter-label"><span class="d-flex align-items-center"><span class="filter-icon icon-car"></span> Sopstveni</span>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>

                        <label class="filter-label"><span class="d-flex align-items-center"><span class="filter-icon icon-bus-mini"></span> Autobuski</span>
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>

                        <label class="filter-label"><span class="d-flex align-items-center"><span class="filter-icon icon-plane"></span> Avionski</span>
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <div class="single-filter-wrapper">
                        <h6>Vrsta usluge:</h6>
                        <label class="filter-label">NA
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>

                        <label class="filter-label">PP
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>

                        <label class="filter-label">ALL
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <div class="filter-btn-wrapper">
                        <button class="btn btn-primary">Prikaži rezultate</button>
                    </div>
                </div>
            </aside>



            <article class="destinacije-wrapper col-md-9">

            <?php
            $currentID = $post->ID;
            $args = array(
                'post_type' => 'page',
                'order' => 'ASC',
                'orderby' => 'title',
                'posts_per_page' => -1, 
                'meta_query'     => array(
                    array(
                        'key'   => '_wp_page_template',
                        'value' => 'templates/template-vile.php'
                    )       
                )
                );
            $the_query = new WP_Query( $args );

            //var_dump($the_query);

            if ( $the_query->have_posts() ) : ?>

                <div class="row">

                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                       
                        <?php
                        // echo get_the_ID();
                        $firstParent = wp_get_post_parent_id(get_the_ID());
                        $secondParent = wp_get_post_parent_id($firstParent);
                        $thirdParent = wp_get_post_parent_id($secondParent);
                        $fourthParent = wp_get_post_parent_id($thirdParent);
                        $fifthParent = wp_get_post_parent_id($fourthParent);
                        $sixthParent = wp_get_post_parent_id($fifthParent);
                        $nizStranica = [$firstParent, $secondParent, $thirdParent, $fourthParent, $fifthParent, $sixthParent];

                        //var_dump($fourthParent);
                        // var_dump($nizStranica);
                        if(in_array($currentID,$nizStranica)) : ?>

                        <?php 
                            $currentId = get_the_ID();
                            $tipSmestaja = get_post_meta($currentId, 'accommodation_type', true);
                            $kategorija = get_post_meta($currentId, 'kategorija', true);
                        ?>

                            <div class="col-md-6 posebno hover1">
                                <?php require('function/prikaz-objekata.php'); ?>
                            </div>

                        <?php endif; ?>
                        
                    <?php endwhile; wp_reset_postdata(); ?>

                </div>

                <?php if ($the_query->max_num_pages > 1) : // check if the max number of pages is greater than 1  ?>
                    <nav class="prev-next-posts">
                        <div class="prev-posts-link">
                            <?php echo get_next_posts_link( 'Older Entries', $the_query->max_num_pages ); // display older posts link ?>
                        </div>
                        <div class="next-posts-link">
                            <?php echo get_previous_posts_link( 'Newer Entries' ); // display newer posts link ?>
                        </div>
                    </nav>
                <?php endif; ?>

            <?php endif; ?>

        </article>
    </div>

<?php //endif; ?>


    </div> <!-- container end -->
</div>


<?php if( $images ): ?>
    <div class="gallery-hidden">
        <ul id="euroturs-galerija" class="list-unstyled euroturs-galerija">
            <?php $slika=1; foreach( $images as $image ): ?>
                <li class="col-xs-6 col-sm-4 col-md-3 gallery-item" data-src="<?php echo esc_url($image['url']); ?>">
                    <a href="">
                        <img class="img-responsive" src="<?php echo esc_url($image['sizes']['thumbnail']); ?>">
                    </a>
                </li>
            <?php $slika++; endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<?php if( have_rows('yt_videos') ): ?>
    <div class="gallery-hidden">
        <div id="video-gallery">
            <?php while( have_rows('yt_videos') ): the_row(); 
                $ytVideo = get_sub_field('yt_url');
                ?>
                <a href="<?php echo $ytVideo; ?>"> </a>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>