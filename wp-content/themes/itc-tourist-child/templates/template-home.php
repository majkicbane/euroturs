<?php

/**
* Template Name: Početna
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header();
?>

	<div id="primary" class="content-area <?php if(get_field('background_image', 'options')) : ?> has-bg-image <?php endif; ?>" <?php if(get_field('background_image', 'options')) : ?> style="background-image:url(<?php the_field('background_image', 'options'); ?>);"<?php endif; ?>>
		<main id="main" class="site-main">

			<?php if(get_field('top_section') == 'Boxes') {
					$boxStyle = get_field('boxes_style');
				} else {
					$sliderStyle = get_field('slider_style');
				}
			?>

			<?php if($boxStyle) : ?>

				<div class="container">
					
					<?php if( have_rows('home_banners') ) : ?>

						<div class="ponude-grid grid-<?php echo $boxStyle; ?>">

							<?php $i=1; while( have_rows('home_banners') ) : the_row();

								$boxPonuda = get_sub_field('ponuda');
								$label1 = get_sub_field('text_label_1');
								$label2 = get_sub_field('text_label_2');
								$opsirnije = get_sub_field('opsirnije');

							?>

							<?php if($boxPonuda) : ?>

								<div class="grid-<?php echo $i; ?>">
									<a href="<?php the_permalink($boxPonuda); ?>">
										<div class="single-ponuda rounded label-top img-zoomin stil-<?php echo $boxStyle; ?>-<?php echo $i; ?>">
											<?php if ( has_post_thumbnail($boxPonuda) ) : ?>
												<?php echo get_the_post_thumbnail($boxPonuda, 'ponude-img' , array( 'class' => 'object-fit-center' ) ); ?>
											<?php endif; ?>

								        	<?php if($label1) : ?>
								        		<span class="box-badge"><?php echo $label1; ?></span>
								        	<?php else : ?>
								        		<span class="box-badge"><?php echo get_the_title( $boxPonuda ); ?></span>
								        	<?php endif; ?>

								        	<?php if($label2) : ?>
								        		<span class="box-promo-text"><?php echo $label2; ?></span>
								        	<?php endif; ?>

								        	<?php if($opsirnije && (($boxStyle == 'style_2' && $i == 1) || ($boxStyle == 'style_2' && $i == 2) || ($boxStyle == 'style_2' && $i == 3) || ($boxStyle == 'style_2' && $i == 4)) || ($boxStyle == 'style_1' && $i == 1) || ($boxStyle == 'style_1' && $i == 5) || ($boxStyle == 'style_1' && $i == 3) || ($boxStyle == 'style_3' && $i == 1) || ($boxStyle == 'style_4' && $i == 1) || ($boxStyle == 'style_4' && $i == 3)) : ?>
								        		<div class="box-promo-opsirnije"><?php echo $opsirnije; ?></div>
								        	<?php endif; ?>
										</div>
									</a>
								</div>

								<?php if($boxStyle == 'style_2' || $boxStyle == 'style_3') : ?>

									<?php if($i == 8 ) {
										break;	
									}  ?>

								<?php elseif ($boxStyle == 'style_1') : ?>

									<?php if($i == 9 ) {
										break;	
									}  ?>

								<?php elseif ($boxStyle == 'style_4') : ?>

									<?php if($i == 10 ) {
										break;	
									}  ?>

								<?php endif; ?>

							<?php endif; ?>

							<?php $i++; endwhile; ?>

						</div>

					<?php endif; ?>

				</div>

			<?php elseif ($sliderStyle) : ?>

			<?php endif; ?>


			<?php /*<div class="filter-wrapper">
				<div class="container">
					<div class="filter-box">
						<h3>Fitler</h3>
					</div>
				</div>
			</div>*/?>


			<?php if( have_rows('homepage_tabs') ) : ?>

				<div class="container">
					<div class="row">
						<div class="col-md-3 lm-home">
							<div class="card rounded">
								<?php if(get_field('promo_offer_title')) : ?>
									<div class="card-header">
								    	<h4><?php the_field('promo_offer_title'); ?></h4>
									</div>
								<?php endif; ?>

								<?php if( have_rows('promo_offers') ) : ?>

									<ul class="list-group list-group-flush">
										<?php while( have_rows('promo_offers') ) : the_row();

								        	$ponuda = get_sub_field('ponuda');
								        	$naziv = get_sub_field('naziv');
								        	$polazak = get_sub_field('polazak');
								        	$nocenja = get_sub_field('br_nocenja');
								        	$prevoz = get_sub_field('prevoz');
								        	$smestaj = get_sub_field('smestaj');
								        	$staraCena = get_sub_field('stara_cena');
								        	$cena = get_sub_field('nova_cena');
								        ?>
									    	<li class="list-group-item single-promo-offer">
									    		<a href="<?php the_permalink($destination); ?>">
										    		<div class="row">
										    			<div class="col-5">
											    			<div class="naziv"><?php echo $naziv; ?></div>
												    		<div class="polazak">Polazak: <?php echo $polazak; ?></div>
												    	</div>

											    		<div class="col-4">
											    			<div class="prevoz">
											    				<?php if($prevoz == 'Sopstveni') : ?>
											    					<span class="icon-car"></span>
											    				<?php elseif($prevoz == 'Bus') : ?>
											    					<span class="icon-bus-mini"></span>
											    				<?php else : ?>
											    					<span class="icon-plane"></span>
											    				<?php endif; ?>
											    			</div>
												    		<div class="nocenja"><?php echo $nocenja; ?></div>
											    		</div>

											    		<div class="col-3">
											    			<div class="cena bold-cena"><?php echo $cena; ?></div>
											    			<div class="cena stara-cena"><?php echo $staraCena; ?></div>
											    		</div>	
											    	</div>
											    </a>
									    	</li>

									    <?php endwhile; ?>
									</ul>
								<?php endif; ?>

								<?php if(get_field('napomena_ponude')) : ?>
								    <div class="description-promo-offer">
								    	<small class="text-muted"><?php the_field('napomena_ponude'); ?></small>
								    </div>
								<?php endif; ?>

								<?php if(!wp_is_mobile()) : ?>
								    <div class="card-footer">
								    	<button class="btn btn-primary">Pogledaj sve ponude</button>
								    </div>
								<?php endif; ?>
							</div>
						</div>

						<div class="col-md-9">
							<div class="home-tabs-wrapper">

								<?php if(get_field('homepage_tabs_title')) : ?>
									<h4><?php the_field('homepage_tabs_title'); ?></h4>
								<?php endif; ?>

								<div class="homepage-tabs-shadow">
									<div class="tabs-header">

										<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">

											<?php $tab=1; while( have_rows('homepage_tabs') ) : the_row();

												$tabTitle = get_sub_field('tab_title');

											?>
												<li class="nav-item">
													<a class="nav-link <?php if($tab==1): ?>active<?php endif; ?>" id="tab-home-<?php echo $tab; ?>" data-toggle="pill" href="#homepage-tab-<?php echo $tab; ?>" role="tab" aria-controls="pills-home" aria-selected="true"><?php echo $tabTitle; ?></a>
												</li>
											<?php $tab++; endwhile; ?>
										</ul>
									</div>

									<div class="tab-content" id="pills-tabContent">

										<?php $tabContent=1; while( have_rows('homepage_tabs') ) : the_row(); ?>

										<?php $row_count = count(get_sub_field('offers')); ?>

											<div class="tab-pane fade <?php if($tabContent==1): ?>show active<?php endif; ?>" id="homepage-tab-<?php echo $tabContent; ?>" role="tabpanel" aria-labelledby="tab-home-<?php echo $tabContent; ?>">

												<?php if( have_rows('offers') ) :  ?>

													<div class="row destinacije-tab">

														<?php if($row_count > 8 && !wp_is_mobile()) : ?>
															
															<div class="owl-carousel owl-theme tab-slider">
																<div class="item">
																	<div class="tab-slider-wrapper">
																		<?php $tabOffer=1; while( have_rows('offers') ) : the_row();

																			$destination = get_sub_field('select_destination');
																			$customTitle = get_sub_field('title');
																			$promoText = get_sub_field('promo_text');
																			$badge = get_sub_field('badge');

																		?>
															    			<?php require('function/tab-objekat.php') ?>

																			<?php if($tabOffer%8=="" && $tabOffer<16) : ?>

																					</div> <!-- close tab wrapper -->
																				</div> <!-- close item -->

																				<div class="item">
																					<div class="tab-slider-wrapper">

																			<?php endif; ?>

																		<?php $tabOffer++; endwhile; ?>
																	</div>
																</div>
															</div>

														<?php else : ?>

															<?php while( have_rows('offers') ) : the_row();

																$destination = get_sub_field('select_destination');
																$customTitle = get_sub_field('title');
																$promoText = get_sub_field('promo_text');
																$badge = get_sub_field('badge');

															?>

																<div class="col-md-3">
															    	<?php require('function/tab-objekat.php') ?>
																</div>

															<?php endwhile; ?>
														<?php endif; ?>

													</div>

														<div class="tab-btn-wrapper">
															<button class="btn btn-primary">Pogledajte ostale ponude</button>

															<div id="customDots"></div>
														</div>

												<?php endif; ?>

											</div>
										<?php $tabContent++; endwhile; ?>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>
			<?php endif; ?>


			<?php
			$args = array(
				'post_type' => 'post',
				'posts_per_page' => 2,
			    'tax_query' => array(
			        array(
			            'taxonomy' => 'category',
			            'field'    => 'id',
			            'terms'    => array(8),
			            'operator' => 'NOT IN',
			        ),
			    ),
			);
			$post_query = new WP_Query($args);

			?>
			<?php if ( $post_query->have_posts() ) : ?>

				<div class="container">
					<div class="novosti-home rounded">

						<div class="novosti-home-title">
							<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">Poslednje objave</a>
						</div>

						<div class="row">
							<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
								<div class="col-md-6">
									<div class="single-blog">
										<div class="row">
											<div class="col-md-4">
												<?php the_post_thumbnail( 'vesti-img' ); ?>
											</div>
											<div class="col-md-8">
												<h4><?php the_title(); ?></h4>
												<div class="novosti-content">
													<?php the_excerpt(); ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endwhile; wp_reset_query() ?>
						</div>
					</div>
				</div>

			<?php endif; ?>


			<?php if( have_rows('o_nama_boxes') ) : ?>
				<div class="o-nama-home">
					<div class="container">

						<?php if(get_field('o_nama_title')) : ?>
							<div class="text-center">
								<h3 class="home-section-title"><?php the_field('o_nama_title'); ?></h3>
							</div>
						<?php endif; ?>

						<div class="owl-carousel owl-theme onama-slider">
							<?php while( have_rows('o_nama_boxes') ) : the_row();

					        	$icon = get_sub_field('icon');
					        	$title = get_sub_field('title');
					        	$content = get_sub_field('content');
					        ?>
						    	<div class="item">
						    		<div class="single-o-nama-box rounded">

						    			<?php if($icon) : ?>
							    			<div class="onama-icon">
							    				<span class="icon-<?php echo $icon; ?>"></span>
							    			</div>
							    		<?php endif; ?>

							    		<h4><?php echo $title; ?></h4>

							    		<div class="single-o-nama-content">
							    			<?php echo $content; ?>
							    		</div>
							    	</div>
						    	</div>

						    <?php endwhile; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>



			<?php if(get_field('destinacije_layout')) : ?>

				<div class="popularne-destinacije-home">

					<?php $boxStyle = get_field('destinacije_layout'); ?>

					<div class="container">

						<?php if(get_field('popularne_destinacije_title')) : ?>
							<div class="text-center">
								<h3 class="home-section-title"><?php the_field('popularne_destinacije_title'); ?></h3>
							</div>
						<?php endif; ?>
						
						<?php if( have_rows('popularne_destinacije') ) : ?>

							<div class="destinacije-grid grid-<?php echo $boxStyle; ?>">

								<?php $i=1; while( have_rows('popularne_destinacije') ) : the_row();

									$destinacija = get_sub_field('destinacija');
									$title = get_sub_field('custom_title');
									$image = get_sub_field('custom_image');

								?>

								<?php if($destinacija) : ?>

									<div class="grid-<?php echo $i; ?>">
										<a href="<?php the_permalink($destinacija); ?>">
											<div class="single-ponuda rounded label-top img-zoomin">
												<?php if ( has_post_thumbnail($destinacija) ) {
													echo get_the_post_thumbnail($destinacija, 'ponude-img' , array( 'class' => 'object-fit-center' ) );
												} ?>

												<div class="destinacije-mask">
													<?php if($title) : ?>
														<h3><span class="icon-location"></span> <?php echo $title; ?></h3>
													<?php else : ?>
														<h3><span class="icon-location"></span> <?php echo get_the_title( $destinacija ); ?></h3>
													<?php endif; ?>
												</div>
											</div>
										</a>
									</div>

									<?php if($boxStyle != 'style_5') : ?>

										<?php if($i == 5 ) {
											break;	
										}  ?>

									<?php endif; ?>

								<?php endif; ?>

								<?php $i++; endwhile; ?>

							</div>

						<?php endif; ?>

					</div>
				</div>

			<?php endif; ?>



			<div class="container">
				<?php

				// Start the Loop.
				// while ( have_posts() ) :
				// 	the_post();

				// 	get_template_part( 'template-parts/content/content', 'page' );

				// endwhile; // End the loop.
				?>
			</div>



		</main><!-- #main -->
	</div><!-- #primary -->


<?php
get_footer();