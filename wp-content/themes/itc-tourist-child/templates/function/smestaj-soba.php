<div class="row">
	<?php if( $gallery ): ?>
		<div class="col-md-3">
			<div class="soba-slike">
				<div class="owl-carousel owl-theme soba-slider">

					<?php $gal=1; foreach( $gallery as $imageGal ): ?>
					    <div class="item">
					    	<?php echo wp_get_attachment_image( $imageGal, 'ponude-img' ); ?>
					    </div>
					<?php $gal++; endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div class="<?php if( $gallery ): ?>col-md-7<?php else : ?>col-md-10<?php endif; ?>">
		<div class="soba-content">
			<h4><?php echo $sobaTitle; ?></h4>

			<?php if($minAdl || $extraBed || $extraKids) : ?>
				<div class="extra-capacity-room">
					<div class="room-capacity">
						<span class="room-capacity-label">Osnovni kapacitet:</span> <span class="room-capacity-value"><?php echo $minAdl; ?></span>
					</div>

					<div class="room-capacity">
						<span class="room-capacity-label">Dopunski kapacitet:</span> Odraslih <span class="room-capacity-value"><?php if($extraBed) : ?><?php echo $extraBed; ?><?php else : ?>0<?php endif; ?></span>

						Dece <span class="room-capacity-value"><?php if($extraKids) : ?><?php echo $extraKids; ?><?php else : ?>0<?php endif; ?></span>
					</div>
				</div>
			<?php endif; ?>
			<div>
				<?php echo $sobaDesc; ?>
			</div>
			
			<?php if( $features ) : ?>
				<div class="sadrzaj">
				    <?php foreach( $features as $feature ): ?>
				        <span><?php echo $feature; ?></span>
				    <?php endforeach; ?>
				</div>
			<?php endif; ?>

		</div>
	</div>
	<div class="col-md-2">
		<div class="soba-cena">
			<div>
				<div class="cena">80-168 € </div>
				<div class="tip-cene">po danu</div>
			</div>
			<button class="btn btn-secondary">Cenovnik</button>
		</div>
	</div>
</div>