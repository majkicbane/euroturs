<div id="share-horizontal" class="hor-share">

  	<ul>
		<li>
			<a href="javascript:void(0)" onclick="javascript:genericSocialShare('http://www.facebook.com/share.php?u=<?php print(urlencode(get_permalink())); ?>&title=<?php print(urlencode(the_title())); ?>')" rel="nofollow"  data-toggle="tooltip" title="Podeli na Facebook-u"><span class="icon-facebook-logo"></span></a>
		</li>
		<li>
			<a href="javascript:void(0)" onclick="javascript:genericSocialShare('http://twitter.com/intent/tweet?status=<?php print(urlencode(the_title())); ?>+<?php print(urlencode(get_permalink())); ?>')" rel="nofollow"  data-toggle="tooltip" title="Podeli na Twitter-u"><span class="icon-twitter-2"></span></a>
		</li>
		<!-- <li>
			<a href="javascript:void(0)" onclick="javascript:genericSocialShare('https://plus.google.com/share?url=<?php print(urlencode(get_permalink())); ?>')" rel="nofollow"><i class="itc-google-plus-circle"></i></a>
		</li> -->
		<!-- <li>
			<a href="javascript:void(0)" onclick="javascript:genericSocialShare('http://www.tumblr.com/share?v=3&u=<?php print(urlencode(get_permalink())); ?>&t=<?php print(urlencode(the_title())); ?>')" rel="nofollow"><i class="itc-tumblr-circle"></i></a>
		</li> -->
		<li>
			<a href="viber://forward?text=<?php print(urlencode(get_permalink())); ?>"  data-toggle="tooltip" title="Pošalji prijatelju Viberom"><span class="icon-phone-call"></span></a>
		</li>
		<!-- <li>
			<a href="whatsapp://send?text=<?php print(urlencode(get_permalink())); ?>"><i class="itc-whatsapp-circle"></i></a>
		</li>
		<li>
			<a href="fb-messenger://share/?link=<?php print(urlencode(get_permalink())); ?>"><i class="itc-messenger-circle"></i></a>
		</li> -->
	</ul>

</div>