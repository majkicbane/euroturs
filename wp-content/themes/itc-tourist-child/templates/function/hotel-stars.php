<span class="label">Hotel:</span> 

<span class="star-icons-wrapper">
    <?php if($kategorija == '5') : ?>
        <span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span>

    <?php elseif($kategorija == '4+') : ?>
        <span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-half-star"></span>

    <?php elseif($kategorija == '4') : ?>
        <span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-empty-star"></span>

    <?php elseif($kategorija == '3+') : ?>
        <span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-half-star"></span><span class="icon-empty-star"></span>

    <?php elseif($kategorija == '3') : ?>
        <span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-empty-star"></span><span class="icon-empty-star"></span>

    <?php elseif($kategorija == '2+') : ?>
        <span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-half-star"></span><span class="icon-empty-star"></span><span class="icon-empty-star"></span>

    <?php elseif($kategorija == '2') : ?>
        <span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-empty-star"></span><span class="icon-empty-star"></span><span class="icon-empty-star"></span>

    <?php else : ?>
        <span class="icon-full-star"></span><span class="icon-empty-star"></span><span class="icon-empty-star"></span><span class="icon-empty-star"></span><span class="icon-empty-star"></span>
    <?php endif; ?>
</span>