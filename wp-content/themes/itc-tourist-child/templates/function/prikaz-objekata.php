<div class="single-destinacija-drzave img-zoomin">
    <div class="row">
        <div class="col-md-6 col-5">
            <figure>
                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail( 'ponude-img' ); ?>
                </a>
            </figure>
        </div>
        <div class="col-md-6 col-7">
            <?php if($tipSmestaja == 'Hotel' && $kategorija) : ?>

                <?php require('hotel-stars.php'); ?>
            
            <?php endif; ?>

            <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>

            <div class="single-app-info-wrapper">
                <div class="single-app-info">
                    <span class="icon-app icon-car"></span> <span>Sopstveni prevoz</span>
                </div>
                <div class="single-app-info">
                    <span class="icon-app icon-date"></span> <span>11 dana / 10 noći</span>
                </div>

                <?php if(get_field('beach_distance') || get_field('gondola_distance') || get_field('center_distance')) : ?>
                    <div class="single-app-info">
                        <span class="icon-app icon-distance"></span> 
                        <?php if(get_field('beach_distance')) : ?>
                            <span>Od plaže: 
                                <?php $beach = get_field('beach_distance');

                                if($beach>=1000) : ?>
                                    <?php echo ($beach / 1000); ?> km
                                <?php else : ?>
                                    <?php echo $beach; ?> m
                                <?php endif; ?>
                            </span>
                        <?php elseif (get_field('gondola_distance')) : ?>
                            <span>Od gondole: 
                                <?php $gondola = get_field('gondola_distance');

                                if($gondola>=1000) : ?>
                                    <?php echo ($gondola / 1000); ?> km
                                <?php else : ?>
                                    <?php echo $gondola; ?> m
                                <?php endif; ?>
                            </span>
                        <?php else : ?>
                            <span>Od centra: 
                                <?php $center = get_field('center_distance');

                                if($center>=1000) : ?>
                                    <?php echo ($center / 1000); ?> km
                                <?php else : ?>
                                    <?php echo $center; ?> m
                                <?php endif; ?>
                            </span>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-8">
    	   <?php 
    		$sadrzaji = get_post_meta(get_the_ID(), 'features', true);

    		if( $sadrzaji ) : ?>

                <b>Sadržaj:</b><br>
                <?php $i=1; foreach( $sadrzaji as $sadrzaj ): ?>
    		        <span><?php echo $sadrzaj; ?><?php if($i!=4) : ?>, <?php endif; ?></span>
    		        <?php if($i==4) : ?>
    		        	...
    		        	<?php break; ?>
    		        <?php endif; ?>
    		    <?php $i++; endforeach; ?>

    	    <?php endif; ?>
        </div>

        <div class="col-4 text-right">
            <div class="smestaj-cena">
                <span class="cena">od 300e</span> / po osobi
            </div>
            <a href="<?php the_permalink(); ?>">
                <button class="btn btn-primary">Detaljnije</button>
            </a>
        </div>
    </div>
</div>