<div class="single-destinacija-tab img-zoomin">
	<figure class="ponuda-img-wrapper rounded">
		<a href="<?php the_permalink($destination); ?>">
			<?php if ( has_post_thumbnail($destination) ) {
				echo get_the_post_thumbnail($destination, 'ponude-img' , array( 'class' => 'object-fit-center' ) );
			} ?>
    	</a>

    	<?php if($badge) : ?>
    		<span class="ponuda-badge"><?php echo $badge; ?></span>
    	<?php endif; ?>

    	<?php if($promoText && !wp_is_mobile()) : ?>
    		<span class="ponuda-promo-text"><?php echo $promoText; ?></span>
    	<?php endif; ?>
    </figure>

	<?php if($promoText && wp_is_mobile()) : ?>
		<span class="ponuda-promo-text-mob"><?php echo $promoText; ?></span>
	<?php endif; ?>

	<?php if($customTitle) : ?>
		<h3><a href="<?php the_permalink($destination); ?>"><?php echo $customTitle; ?></a></h3>
	<?php else : ?>
		<h3><a href="<?php the_permalink($destination); ?>"><?php echo get_the_title($destination); ?></a></h3>
	<?php endif; ?>
</div>