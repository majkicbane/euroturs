<?php
/**
 * Template Name: 3. dubina - Destinacije
 *
 * @package TA Pluton
 */
get_header();
//require('function/search-get.php');
$destSezona = get_field('sezona');
global $wp;
$current_url = home_url(add_query_arg(array(),$wp->request));
 ?>
<!-- Ako ima header slika -->

<script type="text/javascript">

	

</script>

<?php 
global $post;
$currentID = $post->ID;
$pages_no = get_pages("child_of=$currentID&depth=1&parent=$currentID");
$count = count($pages_no);
if( get_field('header_image') ) { ?>
<div id="header-image">
	<img src="<?php the_field('header_image'); ?>" />
	<div class="header-img-mask"></div>
	<div class="destinacija-title">
			<header class="entry-header">
				<div class="row">

					<div class="col-md-12">

<?php if(get_field('iframe_ponude')) : ?>
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
<?php else : ?>
		<?php 
			if($count == 1) {
			the_title( '<h1 class="entry-title">', '</h1> <span class="br-destinacija">('. $count . ' objekat u ponudi)</span>' );
			}
		else if($count > 1 && $count < 5) {
			the_title( '<h1 class="entry-title">', '</h1> <span class="br-destinacija">('. $count . ' objekta u ponudi)</span>' );
			}
		else {
			the_title( '<h1 class="entry-title">', '</h1> <span class="br-destinacija">('. $count . ' objekata u ponudi)</span>' );
		} ?>
<?php endif; ?>
					
					<?php if (function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						} 
					?>
					</div>
				</div>
			</header><!-- .entry-header -->
	</div>
</div>
<div id="destinacija-content" class="ima-header-slika">
<?php } else { ?>
<div id="no-header-image">
	<div class="container">
<div class="destinacija-title">
			<header class="entry-header">
				<div class="row">
					<div class="col-md-12">

<?php if(get_field('iframe_ponude')) : ?>
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
<?php else : ?>
		<?php 
			if($count == 1) {
			the_title( '<h1 class="entry-title">', '</h1> <span class="br-destinacija">('. $count . ' objekat u ponudi)</span>' );
			}
		else if($count > 1 && $count < 5) {
			the_title( '<h1 class="entry-title">', '</h1> <span class="br-destinacija">('. $count . ' objekta u ponudi)</span>' );
			}
		else {
			the_title( '<h1 class="entry-title">', '</h1> <span class="br-destinacija">('. $count . ' objekata u ponudi)</span>' );
		} ?>
<?php endif; ?>
					
					<?php if (function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						} 
					?>
					</div>
				</div>
			</header><!-- .entry-header -->
	</div>
</div>
</div>


<div class="container">
    <?php
    $trenutniParent = get_the_title($post->ID);
    $args = array(
        'post_type' => array( 'page' ),
        'order' => 'ASC',
        'orderby' => 'title',
        'meta_query'     => array(
            array(
                'key'   => '_wp_page_template',
                'value' => 'templates/template-vile.php'
            )       
        )
        );
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
           
            // echo get_the_ID();
            $parent = wp_get_post_parent_id(get_the_ID());
            $grandparent = wp_get_post_parent_id($parent);
            $grandGrandParent = wp_get_post_parent_id($grandparent);
            $nizStranica = [get_the_title($parent), get_the_title($grandparent), get_the_title($grandGrandParent)];
            // var_dump($nizStranica);
            if(in_array($trenutniParent,$nizStranica)){
                echo '<p>'; 
                echo the_title();
                echo get_the_title($grandGrandParent);
                echo '   - ';
                echo get_page_template_slug(); 
                echo '</p>';
            }
            
        }
        wp_reset_postdata();
    }
    ?>
</div>



<div id="destinacija-content" class="nema-header-slika">
<?php } ?>
	<div class="container">
		<div class="row">
		<?php if(get_field('dest_video')) : ?>
			<div class="col-md-8">
		<?php else : ?>
			<div class="col-md-12">
		<?php endif; ?>
				<main id="main" class="site-main" role="main">
					<?php //require('function/share-horizontal.php'); ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', 'destinacije' ); ?>
					<?php endwhile; // end of the loop. ?>
					<?php if(get_field('dest_vodic')) : ?>
						<button class="btn btn-primary"><a href="<?php the_field('dest_vodic'); ?>">Opširnije</a></button>
					<?php endif; ?>
				</main><!-- #main -->
			</div>
		<?php if(get_field('dest_video')) : ?>
			<div class="col-md-4 dest-video">
			<h3>Pogledajte video:</h3>
				<a data-toggle="modal" data-target="#videoFrame">
					<?php echo get_the_post_thumbnail( $currentID, 'destinacije' ); ?>
					<div class="product-video"><span class="icon-youtube2"></span></div>
				</a>
				<?php require('function/destinacije/video.php'); ?>
			</div>
		<?php endif; ?>
		</div>
	</div> <!-- container -->
</div> <!-- destinacija-content -->


<?php if(get_field('iframe_ponude')) : ?>

	<div class="container">
		<?php the_field('iframe_ponude'); ?>
	</div>
	
<?php else : ?>

<div class="container">


		<div class="row">
			<div id="primary" class="col-md-9">
					<form method="get" id="destinacije-forma" class="filteri" action="<?php echo $current_url; ?>/">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php $parent = $post->ID; ?>
			<?php endwhile; endif; ?>
			<?php
  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
			$dest = (array(
				'post_type'=>'page',
				'post_parent'=>$parent,
				'posts_per_page'=>12,
            	'meta_query' => $dodaci,
                'order' => $order,
                'orderby' => $orderby,
                'meta_key' => $sort,
				'paged' => $paged,
				// 'meta_value' => 'Privatni',
			 //    'meta_compare' => 'LIKE',
			));
            $dest = new WP_Query($dest);
			?>
<?php  if($dest->have_posts()) : ?>
	<div class="page" id="p<?php echo $paged; ?>">
		<div class="row destinacije">
			<div class="col-md-8 destinacije-naslov">
				<h2>Objekti u ponudi:</h2>
			</div>
			<div class="col-md-4 footer-right">
				<?php //require('function/destinacije/sortiranje-apartmani.php'); ?>
			</div>
		</div>
		<div class="row destinacije destinacije-listing">     
			<?php while ($dest->have_posts()) : $dest->the_post(); ?>
				<?php require('function/prikaz-objekata.php'); ?>
			<?php endwhile; wp_reset_postdata(); ?>
			<?php if ($dest->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
			  <nav class="prev-next-posts">
			    <div class="prev-posts-link">
			      <?php echo get_next_posts_link( 'Older Entries', $dest->max_num_pages ); // display older posts link ?>
			    </div>
			    <div class="next-posts-link">
			      <?php echo get_previous_posts_link( 'Newer Entries' ); // display newer posts link ?>
			    </div>
			  </nav>
			<?php } ?>
		</div>
	</div>
<?php endif; ?>
		</div><!-- #primary -->
		<div id="secondary" class='col-md-3 filteri-home'>
		    <div class="pozadina-filtera">
		        <?php //require('function/destinacije/destinacija-search.php'); ?>
		    </div>

		    <?php if (is_page(86286)) : ?>
				<a class="weatherwidget-io" href="https://forecast7.com/en/41d8423d49/bansko/" data-label_1="BANSKO" data-label_2="WEATHER" data-days="5" style="display: block; position: relative; height: 409px; padding: 0px; overflow: hidden; text-align: left; text-indent: -299rem;">BANSKO WEATHER<iframe id="weatherwidget-io-0" class="weatherwidget-io-frame" scrolling="no" frameborder="0" width="100%" src="https://weatherwidget.io/w/" style="display: block; position: absolute; top: 0px; height: 409px;"></iframe></a>
				<script>
				!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
				</script>

				<br>
				<br>


				<a href="https://banskozimovanje.rs/#webcams" target="_blank">

					<h3>Uživo kamere</h3>
					
					<img src="<?php echo get_template_directory_uri(); ?>/images/uzivo-kamere.jpg" alt="Uzivo kamere Bansko">
				</a>

			<?php else : ?>



		    <?php endif; ?>




		</div>
		</form>
	</div>
</div> <!-- container -->

<?php endif; ?>


<?php get_footer(); ?>