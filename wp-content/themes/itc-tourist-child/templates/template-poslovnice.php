<?php
/**
 * Template Name: Poslovnice
 *
 * @package Euroturs
 */

get_header();
?>

<!-- Ako ima header slika -->
<div id="<?php if( get_field('header_image') ) : ?>header-image<?php else : ?>no-header-image<?php endif; ?>">
	<?php if( get_field('header_image') ) : ?>
		<img src="<?php the_field('header_image'); ?>" />
	<?php endif; ?>

		<div class="header-img-mask">
			<div class="container">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			
				<?php if (function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} 
				?>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : the_post(); ?>

				

				<div class="container">
					<div class="row">
						<?php if(get_field('alt_tekst')) : ?>
							<div class="col-md-8">
						<?php else : ?>
							<div class="col-md-12">
						<?php endif; ?>

								<div class="entry-content">
									<?php
									the_content();

									wp_link_pages(
										array(
											'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
											'after'  => '</div>',
										)
									);
									?>
								</div><!-- .entry-content -->

							</div>

						<?php if(get_field('alt_tekst')) : ?>
							<div class="col-md-4">
								<div class="entry-content">
									<div class="alt-text-onama rounded">
										<?php the_field('alt_tekst'); ?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>


				<?php if( have_rows('poslovnice', 'options') ): ?>
				    <div class="container">

						<div class="entry-content">
					    	<div class="row">
					    		<?php while( have_rows('poslovnice', 'options') ): the_row(); 
							        $grad = get_sub_field('grad');
							        $adresa = get_sub_field('adresa');
							        $email = get_sub_field('contact_email');
							        $telefoni = get_sub_field('telefoni');
							        $mon_fri = get_sub_field('mon_fri');
							        $sat = get_sub_field('sat');
							        $sun = get_sub_field('sun');
							        $image = get_sub_field('slika');
							    ?>
							    
							    
								<div class="col-md-6 mb-4">
									<div class="single-posao single-o-nama-box">
								        <div class="single-header-poslovnica img-zoomin">
								        	<div class="row">
								        		<div class="col-md-5">
								        			<?php if( $image ) : ?>
													    <figure class="ponuda-img-wrapper rounded"><?php echo wp_get_attachment_image( $image, 'ponude-img' ); ?></figure>
													<?php endif; ?>
								        		</div>
								        		<div class="col-md-7">
										        	<h4><span class="poslovnica-grad"><?php echo $grad; ?></span></h4>
										        	<span class="poslovnica-adresa"><?php echo $adresa; ?></span>
										        	<span class="poslovnica-telefoni"><?php echo $telefoni; ?></span>
										        	<span class="poslovnica-email"><?php echo $email; ?></span>
										        	<div>
										        		<h5>Radno vreme:</h5>
										        		<span class="poslovnica-rvreme">
										        			<span class="rvreme-label">Pon-Pet</span> <span class="rvreme-vrednost"><?php echo $mon_fri; ?></span>
										        		</span>
											        	<span class="poslovnica-rvreme">
											        		<span class="rvreme-label">Subota</span> <span class="rvreme-vrednost"><?php echo $sat; ?></span>
											        	</span>
											        	<span class="poslovnica-rvreme">
											        		<span class="rvreme-label">Nedelja</span> <span class="rvreme-vrednost"><?php echo $sun; ?></span>
											        	</span>
											        </div>
											    </div>
											</div>
								        </div>
								    </div>
								</div>

							    <?php endwhile; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>

							<?php if ( get_edit_post_link() ) : ?>
								<footer class="entry-footer">
									<?php
									edit_post_link(
										sprintf(
											wp_kses(
												/* translators: %s: Post title. Only visible to screen readers. */
												__( 'Edit <span class="screen-reader-text">%s</span>', 'twentynineteen' ),
												array(
													'span' => array(
														'class' => array(),
													),
												)
											),
											get_the_title()
										),
										'<span class="edit-link">' . twentynineteen_get_icon_svg( 'edit', 16 ),
										'</span>'
									);
									?>
								</footer><!-- .entry-footer -->
							<?php endif; ?>

			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
