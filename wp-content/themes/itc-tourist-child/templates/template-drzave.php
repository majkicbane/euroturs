<?php
/**
 * Template Name: 2. dubina - Drzave
 *
 * @package TA Pluton
 */

global $post;
$currentID = $post->ID;
$pages_no = get_pages("child_of=$currentID&depth=1&parent=$currentID");
$count = count($pages_no);

get_header(); ?>

<!-- Ako ima header slika -->
<?php if( get_field('header_image') ) : ?>

	<div id="header-image">
		<img src="<?php the_field('header_image'); ?>" />

		<div class="header-img-mask">
			<div class="container">
				<div class="row align-items-end">
					<div class="col-md-9">
						<?php if($count > 1 && $count < 5) {
							the_title( '<span class="br-destinacija">'. $count . '</span> destinacije u ponudi<h1 class="entry-title">', '</h1>' );
							}
						else {
							the_title( ' <span class="br-destinacija">'. $count . '</span> destinacija u ponudi<h1 class="entry-title">', '</h1>' );
						} ?>
					
						<?php if (function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p id="breadcrumbs">','</p>');
							} 
						?>
					</div>
					<div class="col-md-3">
						<div class="header-links">
							<?php $images = get_field('galerija'); ?>

							<?php if( $images ): ?>
							    <?php $slika=1; foreach( $images as $image ): ?>
								    <?php if($slika==1) : ?>
								    	<div>
								    		<a href="<?php echo esc_url($image['url']); ?>" data-rl_caption="" data-rel="lightbox-gallery-destinacije">
							                    <span class="icon-large icon-picture"></span> <span>Pogledajte galeriju fotografija</span>
							                </a>
							            </div>
								    <?php endif; ?>
								<?php $slika++; endforeach; ?>
							<?php endif; ?>

							<?php if(get_field('destinacija_video')) : ?>
								<div>
									<a href="">
										<span class="icon-large icon-video"></span> <span>Pogledajte video</span>
									</a>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>

<?php endif; ?>

<div class="main-content">
	<div class="container">

		<div class="site-content destinacija-opis" role="main">
			<div class="row">
				<?php while ( have_posts() ) : the_post(); ?>
					<div class="col-md-9">
						<div class="drzava-short-conent">
							<?php the_field('kratki_opis'); ?>

							<div class="collapse" id="citaj-sve">
							    <?php the_content(); ?>
							</div>

							<div class="add-overlay light-overlay">
								<a class="read-more-opis" data-toggle="collapse" href="#citaj-sve" role="button" aria-expanded="false" aria-controls="collapseExample">Čitaj opširnije</a>
							</div>
						</div>

					</div>

					<?php $parent = $post->ID; ?>

					<?php //var_dump($parent); ?>
				<?php endwhile; // end of the loop. ?>

				<div class="col-md-3">
					<div class="mapa-lokacija">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mapa.png">
						<div class="map-label">
							Pogledaj mapu
						</div>
					</div>
				</div>
			</div>
		</div><!-- #main -->

		<div class="container">
    <?php
    $trenutniParent = get_the_title($post->ID);
    $args = array(
        'post_type' => array( 'page' ),
        'order' => 'ASC',
        'orderby' => 'title',
        'meta_query'     => array(
            array(
                'key'   => '_wp_page_template',
                'value' => 'templates/template-vile.php'
            )       
        )
        );
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
           
            // echo get_the_ID();
            $parent = wp_get_post_parent_id(get_the_ID());
            $grandparent = wp_get_post_parent_id($parent);
            $grandGrandParent = wp_get_post_parent_id($grandparent);
            $nizStranica = [get_the_title($parent), get_the_title($grandparent), get_the_title($grandGrandParent)];
            // var_dump($nizStranica);
            if(in_array($trenutniParent,$nizStranica)){
                echo '<p>'; 
                echo the_title();
                echo get_the_title($grandGrandParent);
                echo '   - ';
                echo get_page_template_slug(); 
                echo '</p>';
            }
            
        }
        wp_reset_postdata();
    }
    ?>
</div>


		<?php $subpages= (array(
			'post_type'=>'page',
			'post_status' => 'publish',
			'order'=>'ASC',
			'post_parent'=>$parent,
			'posts_per_page' => -1
		));
			$subpages = new WP_Query($subpages);
		?>

		<?php if($subpages->have_posts()) : ?>
			<?php while ($subpages->have_posts()) : $subpages->the_post(); ?>
				


			<?php endwhile; ?>
		<?php endif; ?>





		<?php $dest= (array(
			'post_type'=>'page',
			'post_status' => 'publish',
			'orderby'=>'menu_order',
			'order'=>'ASC',
			'post_parent__in'=>array(14),
			'posts_per_page' => 12, 
			'paged' => $paged,
	        'meta_query' => array(
	            array(
	                'key' => '_wp_page_template',
	                'value' => 'templates/template-vile.php', // template name as stored in the dB
	            )
	        )
		));
			$dest = new WP_Query($dest);
		?>

		<?php if($dest->have_posts()) : ?>

			<div class="row">

				<aside class="col-md-3">
					<div class="drzava-filteri">

						<div class="single-filter-wrapper">
							<h6>Tip smestaja:</h6>
							<label class="filter-label">Hotel
								<input type="checkbox" checked="checked">
								<span class="checkmark"></span>
							</label>

							<label class="filter-label">Apartman
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>
						</div>

						<div class="single-filter-wrapper filter-destinacija">
							<h6>Destinacija:</h6>
							<label class="filter-label">Asprovalta
								<input type="checkbox">
							 	<span class="checkmark"></span>
							</label>

							<label class="filter-label">Pefkohori
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>
							<label class="filter-label">Krf
								<input type="checkbox">
							 	<span class="checkmark"></span>
							</label>

							<label class="filter-label">Paralija
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>
							<label class="filter-label">Krit
								<input type="checkbox">
							 	<span class="checkmark"></span>
							</label>

							<label class="filter-label">Tasos
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>
							<label class="filter-label">Asprovalta
								<input type="checkbox">
							 	<span class="checkmark"></span>
							</label>

							<label class="filter-label">Pefkohori
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>
							<label class="filter-label">Krf
								<input type="checkbox">
							 	<span class="checkmark"></span>
							</label>

							<label class="filter-label">Paralija
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>
							<label class="filter-label">Krit
								<input type="checkbox">
							 	<span class="checkmark"></span>
							</label>

							<label class="filter-label">Tasos
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>
						</div>

						<div class="single-filter-wrapper">
							<h6>Tip prevoza:</h6>
							<label class="filter-label"><span class="d-flex align-items-center"><span class="filter-icon icon-car"></span> Sopstveni</span>
								<input type="checkbox" checked="checked">
								<span class="checkmark"></span>
							</label>

							<label class="filter-label"><span class="d-flex align-items-center"><span class="filter-icon icon-bus-mini"></span> Autobuski</span>
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>

							<label class="filter-label"><span class="d-flex align-items-center"><span class="filter-icon icon-plane"></span> Avionski</span>
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>
						</div>

						<div class="single-filter-wrapper">
							<h6>Vrsta usluge:</h6>
							<label class="filter-label">NA
								<input type="checkbox" checked="checked">
								<span class="checkmark"></span>
							</label>

							<label class="filter-label">PP
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>

							<label class="filter-label">ALL
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>
						</div>

						<div class="filter-btn-wrapper">
							<button class="btn btn-primary">Prikaži rezultate</button>
						</div>
					</div>
				</aside>

				<article class="destinacije-wrapper col-md-9">

					<div class="row destinacije destinacije-listing">
						<?php while ($dest->have_posts()) : $dest->the_post(); ?>
							<div class="col-md-6 posebno hover1">    	
						    	<div class="single-destinacija-drzave img-zoomin">
						    		<div class="row">
						    			<div class="col-6">
							        		<figure>
								        		<a href="http://localhost/euroturs/leto/grcka/grcka-hoteli/tasos/apartmani-green-gardens-polihrono/">
									        		<?php the_post_thumbnail( 'ponude-img' ); ?>
									        	</a>
									        </figure>
									    </div>
									    <div class="col-6">
									    	<span class="tip-smestaja">
										    		Hotel <span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span><span class="icon-full-star"></span>
										    </span>
											<a href="http://localhost/euroturs/leto/grcka/grcka-hoteli/tasos/apartmani-green-gardens-polihrono/"><h3><?php the_title(); ?></h3></a>

											<div class="single-app-info-wrapper">
												<div class="single-app-info">
													<span class="icon-app icon-car"></span> <span>Sopstveni prevoz</span>
												</div>
												<div class="single-app-info">
													<span class="icon-app icon-date"></span> <span>11 dana / 10 noći</span>
												</div>
												<div class="single-app-info">
													<span class="icon-app icon-distance"></span> <span>Od mora: 100m</span>
												</div>
											</div>
									    </div>
									</div>

									<div class="row mt-2">
										<div class="col-8">
											<b>Sadržaj:</b><br>
											wi-fi, tv, parking, dozvoljeni ljubimci
										</div>
										<div class="col-4 text-right">
											<div class="smestaj-cena">
												<span class="cena">od 300e</span> / po osobi
											</div>
											<button class="btn btn-primary">Detaljnije</button>
										</div>
									</div>
								</div>
							</div>
						<?php endwhile; wp_reset_postdata(); ?>

						<?php if ($dest->max_num_pages > 1) : // check if the max number of pages is greater than 1  ?>
							<nav class="prev-next-posts">
								<div class="prev-posts-link">
									<?php echo get_next_posts_link( 'Older Entries', $dest->max_num_pages ); // display older posts link ?>
								</div>
							    <div class="next-posts-link">
									<?php echo get_previous_posts_link( 'Newer Entries' ); // display newer posts link ?>
							    </div>
							</nav>
						<?php endif; ?>
					</div>

				</article>
			</div>

		<?php endif; ?>

	</div> <!-- container end -->
</div>



<?php if( $images ): ?>
    <ul class="gallery gallery-hidden">
        <?php $slika=1; foreach( $images as $image ): ?>
	        <?php if($slika>1) : ?>
	            <li>
	                <a href="<?php echo esc_url($image['url']); ?>" class="rl-gallery-link" data-rl_caption="" data-rel="lightbox-gallery-destinacije">
	                     <img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
	                </a>
	            </li>
	        <?php endif; ?>
        <?php $slika++; endforeach; ?>
    </ul>
<?php endif; ?>

<?php get_footer(); ?>