<?php
/**
 * Template Name: Kontakt
 *
 * @package Euroturs
 */

get_header();
?>

<!-- Ako ima header slika -->
<div id="<?php if( get_field('header_image') ) : ?>header-image<?php else : ?>no-header-image<?php endif; ?>">
	<?php if( get_field('header_image') ) : ?>
		<img src="<?php the_field('header_image'); ?>" />
	<?php endif; ?>

		<div class="header-img-mask">
			<div class="container">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			
				<?php if (function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} 
				?>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="container">
					<div class="row">
						<div class="col-md-6">

							<div class="entry-content">
								<?php
								the_content();

								wp_link_pages(
									array(
										'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
										'after'  => '</div>',
									)
								);
								?>
							</div><!-- .entry-content -->

						</div>
						<div class="col-md-6">
							<div class="kontakt-img">
								
								<?php 
								$images = get_field('galerija');
								if( $images ): ?>
								    <ul class="gallery">
								        <?php foreach( $images as $image ): ?>
								            <li>
				                				<a href="<?php echo esc_url($image['url']); ?>" class="rl-gallery-link" data-rl_caption="" data-rel="lightbox-gallery-bus">
								                     <img src="<?php echo esc_url($image['sizes']['ponude-img']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
								                </a>
								            </li>
								        <?php endforeach; ?>
								    </ul>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>

				<?php if( have_rows('poslovnice', 'options') ): ?>
					<div class="onama-poslovnice">
					    <div class="container">

					    	<div class="text-center">
					    		<h3>Naše poslovnice</h3>
					    	</div>

					    	<div class="row">
					    		<?php while( have_rows('poslovnice', 'options') ): the_row(); 
							        $grad = get_sub_field('grad');
							        $adresa = get_sub_field('adresa');
							        $email = get_sub_field('contact_email');
							        $telefoni = get_sub_field('telefoni');
							        $mon_fri = get_sub_field('mon_fri');
							        $sat = get_sub_field('sat');
							        $sun = get_sub_field('sun');
							    ?>
							    
							    <div class="col-md-3">
							        <div class="single-header-poslovnica">
							        	<h4><span class="poslovnica-grad"><?php echo $grad; ?></span></h4>
							        	<span class="poslovnica-adresa"><?php echo $adresa; ?></span>
							        	<span class="poslovnica-telefoni"><?php echo $telefoni; ?></span>
							        	<span class="poslovnica-email"><?php echo $email; ?></span>
							        </div>
							    </div>

							    <?php endwhile; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>


				<?php if(get_field('reklamacije')) : ?>
					<div class="reklamacije-sekcija mt-4">
						<div class="container">

					    	<div class="text-center">
					    		<h3 class="home-section-title">REKLAMACIJE</h3>
					    	</div>

							<p>
							  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
							    <?php the_field('reklamacije_title'); ?>
							  </button>
							</p>
							<div class="collapse" id="collapseExample">
							  <div class="card card-body">
							    <?php the_field('reklamacije'); ?>
							  </div>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<?php if ( get_edit_post_link() ) : ?>
					<footer class="entry-footer">
						<?php
						edit_post_link(
							sprintf(
								wp_kses(
									/* translators: %s: Post title. Only visible to screen readers. */
									__( 'Edit <span class="screen-reader-text">%s</span>', 'twentynineteen' ),
									array(
										'span' => array(
											'class' => array(),
										),
									)
								),
								get_the_title()
							),
							'<span class="edit-link">' . twentynineteen_get_icon_svg( 'edit', 16 ),
							'</span>'
						);
						?>
					</footer><!-- .entry-footer -->
				<?php endif; ?>

			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
