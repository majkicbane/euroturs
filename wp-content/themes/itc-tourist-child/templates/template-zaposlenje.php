<?php
/**
 * Template Name: Zaposlenje
 *
 * @package Euroturs
 */

get_header();
?>

<!-- Ako ima header slika -->

<div id="<?php if( get_field('header_image') ) : ?>header-image<?php else : ?>no-header-image<?php endif; ?>">
	<?php if( get_field('header_image') ) : ?>
		<img src="<?php the_field('header_image'); ?>" />
	<?php endif; ?>

		<div class="header-img-mask">
			<div class="container">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			
				<?php if (function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} 
				?>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="container">
					<div class="row">
						<?php if(get_field('alt_tekst')) : ?>
							<div class="col-md-8">
						<?php else : ?>
							<div class="col-md-12">
						<?php endif; ?>

								<div class="entry-content">
									<?php
									the_content();

									wp_link_pages(
										array(
											'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
											'after'  => '</div>',
										)
									);
									?>
								</div><!-- .entry-content -->

							</div>

						<?php if(get_field('alt_tekst')) : ?>
							<div class="col-md-4">
								<div class="entry-content">
									<div class="alt-text-onama rounded">
										<?php the_field('alt_tekst'); ?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>


				<?php
				$args = array(
					'post_type' => 'poslovi',
					'posts_per_page' => -1
				);
				$post_query = new WP_Query($args);

				?>
				<?php if ( $post_query->have_posts() ) : ?>

					<div class="container">
						<div class="entry-content">
							<div class="zaposlenje-title">
								<h3>Aktuelni poslovi</h3>
							</div>

							<div class="row">
								<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
									<div class="col-md-6 mb-4">
										<div class="single-posao single-o-nama-box">
											<h4><?php the_title(); ?></h4>
											<div class="zaposlenje-content">
												<?php the_content(); ?>
											</div>
										</div>
									</div>
								<?php endwhile; wp_reset_query() ?>
							</div>
						</div>
					</div>

				<?php endif; ?>

				<?php if ( get_edit_post_link() ) : ?>
					<footer class="entry-footer">
						<?php
						edit_post_link(
							sprintf(
								wp_kses(
									/* translators: %s: Post title. Only visible to screen readers. */
									__( 'Edit <span class="screen-reader-text">%s</span>', 'twentynineteen' ),
									array(
										'span' => array(
											'class' => array(),
										),
									)
								),
								get_the_title()
							),
							'<span class="edit-link">' . twentynineteen_get_icon_svg( 'edit', 16 ),
							'</span>'
						);
						?>
					</footer><!-- .entry-footer -->
				<?php endif; ?>

			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
