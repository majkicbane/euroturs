<?php
/**
 * Template Name: Download
 *
 * @package TA Pluton
 */
require_once __DIR__ . '/vendor/autoload.php';
//echo $_GET['id'];
$idposta=$_GET['id'];
$url = get_site_url($idposta);
$content_post 	= 	get_post($idposta);
$title 			=	$content_post->post_title;
$content 		= 	$content_post->post_content;
$cenovnik 		= 	get_field('cenovnik', $idposta);
$nacin_placanja = 	get_field('nacin_placanja', $idposta);
$organizator 	= 	get_field('organizator_puta', $idposta);
$skipass 	= 	get_field('ski_pass', $idposta);
$busprevoz 	= 	get_field('autobuski_prevoz', $idposta);
$napomene 	= 	get_field('napomene', $idposta);
$okompaniji 	= 	get_field('o_kompaniji', $idposta);
$prtljag 	= 	get_field('prtljag', $idposta);
$cenametropole 		= 	get_field('cena_metropole', $idposta);
$nacin_placanja_metropole = 	get_field('nacin_placanja_2', $idposta);
$ukljuceno 		= 	get_field('ukljuceno', $idposta);
$nije_ukljuceno 		= 	get_field('nije_ukljuceno', $idposta);
$izleti 		= 	get_field('fakultativni_izleti_metropole', $idposta);
$program 		= 	get_field('program_putovanja_metropole', $idposta);
$metropolaOrganizator 		= 	get_field('organizator_puta_metropole', $idposta);
$doplatepopusti 		= 	get_field('doplate_i_popusti_metropole', $idposta);
//the_field('cenovnik', $idposta);
//generate html-pdf content
$html_pdf_content.="
<img src='http://localhost/euroturs/wp-content/themes/itc-tourist-child/img/pdf-header.png'>
<h1>{$title}</h1>";

if ($program) {
	$html_pdf_content.="
	<h2> Program putovanja </h2>
		<div>
			{$program}
		</div>
	";
}
if ($content) {
	$html_pdf_content.="
	<h2> Opis </h2>
		<div>
			{$content}
		</div>
	";
}
if ($okompaniji) {
	$html_pdf_content.="
	<h2> O kompaniji </h2>
		<div>
			{$okompaniji}
		</div>
	";
}
if ($prtljag) {
	$html_pdf_content.="
	<h2> Prtljag </h2>
		<div>
			{$prtljag}
		</div>
	";
}
if (!$cenametropole) {
	if ($cenovnik) {
		$html_pdf_content.="
		<h2> Cenovnik </h2>
			<div>
				{$cenovnik}
			</div>
		";
	}
}
if ($cenametropole) {
	$html_pdf_content.="
	<h2> Cenovnik </h2>
		<div>
			{$cenametropole}
		</div>
	";
}
if ($ukljuceno) {
	$html_pdf_content.="
	<h2> Aranžman obuhvata </h2>
		<div>
			{$ukljuceno}
		</div>
	";
}
if ($nije_ukljuceno) {
	$html_pdf_content.="
	<h2> Aranžman ne obuhvata </h2>
		<div>
			{$nije_ukljuceno}
		</div>
	";
}
if ($nacin_placanja) {
	$html_pdf_content.="
	<h2> Način plaćanja </h2>
	<div>
		{$nacin_placanja}
	</div>
	";
}
if ($nacin_placanja_metropole) {
	$html_pdf_content.="
	<h2> Način plaćanja </h2>
	<div>
		{$nacin_placanja_metropole}
	</div>
	";
}
if ($skipass) {
	$html_pdf_content.="
	<h2> Ski pass </h2>
	<div>
		{$skipass}
	</div>
	";
}
if ($busprevoz) {
	$html_pdf_content.="
	<h2> Autobuski prevoz </h2>
	<div>
		{$busprevoz}
	</div>
	";
}
if ($izleti) {
	$html_pdf_content.="
	<h2> Fakultativni izleti </h2>
	<div>
		{$izleti}
	</div>
	";
}
if ($napomene) {
	$html_pdf_content.="
	<h2> Napomene </h2>
	<div>
		{$napomene}
	</div>
	";
}
if ($doplatepopusti) {
	$html_pdf_content.="
	<h2> Ostalo </h2>
	<div>
		{$doplatepopusti}
	</div>
	";
}
if ($organizator) {
	$html_pdf_content.="
	<h2> Organizator puta </h2>
	<div>
		{$organizator}
	</div>
	";
}
if ($metropolaOrganizator) {
	$html_pdf_content.="
	<h2> Organizator puta </h2>
	<div>
		{$metropolaOrganizator}
	</div>
	";
}
//generate html pdf content end
@$mpdf = new mPDF();
@$mpdf->WriteHTML("
{$html_pdf_content}
<style>
</style>
");
@$mpdf->Output();
?>