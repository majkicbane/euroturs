<?php

$aviokarte = new CPT( array(
    'post_type_name' => 'aviokarte',
    'singular'       => __('Avio karta', 'euroturs'),
    'plural'         => __('Avio karte', 'euroturs'),
    'slug'           => 'avio-karta'
),

	array(
    'show_in_rest' => true,
    'supports'  => array( 'custom-fields' ),
    'publicly_queryable'  => false,
    'menu_icon' => 'dashicons-tickets'
));