<?php

$poslovi = new CPT( array(
    'post_type_name' => 'poslovi',
    'singular'       => __('Posao', 'euroturs'),
    'plural'         => __('Poslovi', 'euroturs'),
    'slug'           => 'poslovi'
),

	array(
    'show_in_rest' => true,
    'supports'  => array( 'title', 'editor', 'custom-fields'),
    'publicly_queryable'  => false,
    'menu_icon' => 'dashicons-admin-home'
));