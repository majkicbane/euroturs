<?php

$autobusi = new CPT( array(
    'post_type_name' => 'autobusi',
    'singular'       => __('Autobus', 'euroturs'),
    'plural'         => __('Autobusi', 'euroturs'),
    'slug'           => 'autobusi'
),

	array(
    'show_in_rest' => true,
    'supports'  => array( 'title', 'editor', 'custom-fields', 'excerpt', 'thumbnail' ),
    'menu_icon' => 'dashicons-admin-post'
));