<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('row single-vest align-items-center'); ?>>
	<div class="col-md-4">
		<div class=" vest-featured-img"><?php twentynineteen_post_thumbnail(); ?></div>
	</div>
	<div class="col-md-8">
		<header class="entry-header">
			<?php
			if ( is_sticky() && is_home() && ! is_paged() ) {
				printf( '<span class="sticky-post">%s</span>', _x( 'Featured', 'post', 'twentynineteen' ) );
			}
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( sprintf( '<h3 class="entry-title">', esc_url( get_permalink() ) ), '</h3>' );
			endif;
			?>

			<div class="vest-info">
				<div><span class="icon-date"></span> <?php the_date('d.m.Y.'); ?></div> <div><span class="icon-clock"></span> <?php the_time( 'G:i' ); ?></div>
			</div>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php
			the_content(
				sprintf(
					wp_kses(
						/* translators: %s: Post title. Only visible to screen readers. */
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentynineteen' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			?>
		</div><!-- .entry-content -->
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
