<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">

		<div class="entry-content">
			<div class="row">
				<div class="col-md-4">
					<?php
					the_content(
						sprintf(
							wp_kses(
								/* translators: %s: Post title. Only visible to screen readers. */
								__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentynineteen' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							get_the_title()
						)
					);

					wp_link_pages(
						array(
							'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
							'after'  => '</div>',
						)
					);
					?>
				</div>
				<div class="col-md-8">

					<?php 
					$images = get_field('bus_galerija');
					if( $images ): ?>

					<div class="gallery gallery-size-thumbnail" data-pgc-sgb-id="cl0">
					    <ul class="gallery">
					        <?php foreach( $images as $image ): ?>
					            <li>
	                				<a href="<?php echo esc_url($image['url']); ?>" class="rl-gallery-link" data-rl_caption="" data-rel="lightbox-gallery-bus">
					                     <img src="<?php echo esc_url($image['sizes']['ponude-img']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
					                </a>
					            </li>
					        <?php endforeach; ?>
					    </ul>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div><!-- .entry-content -->

		<footer class="entry-footer">
			<?php twentynineteen_entry_footer(); ?>
		</footer><!-- .entry-footer -->

		<?php if ( ! is_singular( 'attachment' ) ) : ?>
			<?php get_template_part( 'template-parts/post/author', 'bio' ); ?>
		<?php endif; ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
