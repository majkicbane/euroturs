<?php
/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php /*<div class="col-md-4">
		<?php twentynineteen_post_thumbnail(); ?>
	</div>*/?>

	<div class="single-aviokarta single-katalog-list rounded">
		<div class="row">
			<div class="col-3">
				<!-- <div class="katalog-icon">
					<span class="icon-pdf-file"></span>
				</div> -->
				<?php the_post_thumbnail( 'katalog-img' ); ?>
			</div>
			<div class="col-9 d-flex flex-column justify-content-between">
				<div class="katalog-wrapper">
					<header class="entry-header">
						<?php
						if ( is_sticky() && is_home() && ! is_paged() ) {
							printf( '<span class="sticky-post">%s</span>', _x( 'Featured', 'post', 'twentynineteen' ) );
						}
						the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
						?>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_excerpt(); ?>
					</div><!-- .entry-content -->
				</div>

				<div class="katalog-actions d-flex justify-content-end">
					<a href="<?php the_field('katalog'); ?>" download>
						<span class="icon-download-button"></span> Preuzmi PDF
					</a>
				</div>
			</div>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
