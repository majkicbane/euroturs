<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

<!-- Ako ima header slika -->
<div id="<?php if( get_field('header_image') ) : ?>header-image<?php else : ?>no-header-image<?php endif; ?>">
	<?php if( get_field('header_image') ) : ?>
		<img src="<?php the_field('header_image'); ?>" />
	<?php endif; ?>

		<div class="header-img-mask">
			<div class="container">
				<h1 class="entry-title"><?php single_post_title(); ?></h1>
			
				<?php if (function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} 
				?>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div id="primary" class="content-area col-md-9">
				<main id="main" class="site-main">

					<?php

					query_posts($query_string . '&cat=-8');

					if ( have_posts() ) {

						// Load posts loop.
						while ( have_posts() ) {
							the_post();
							get_template_part( 'template-parts/content/content' );
						}

						// Previous/next page navigation.
						twentynineteen_the_posts_navigation();

					} else {

						// If no content, include the "No posts found" template.
						get_template_part( 'template-parts/content/content', 'none' );

					}
					?>

				</main><!-- .site-main -->
			</div><!-- .content-area -->
			<div id="secondary" class="col-md-3">
				<?php
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 3,
				    'tax_query' => array(
				        array(
				            'taxonomy' => 'category',
				            'field'    => 'id',
				            'terms'    => array(8)
				        ),
				    ),
				);
				$post_query = new WP_Query($args);

				?>
				<?php if ( $post_query->have_posts() ) : ?>

					<div class="container">
						<div class="novosti-home rounded">

							<div class="novosti-side-title">
								Katalozi
							</div>

							<div class="row">
								<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
									<div class="col-md-12">
										<div class="single-blog">
											<a href="<?php the_permalink(); ?>">
												<div class="row">
													<div class="col-md-2">
														<span class="icon-pdf-file"></span>
													</div>
													<div class="col-md-10">
														<h4><?php the_title(); ?></h4>
													</div>
												</div>
											</a>
										</div>
									</div>
								<?php endwhile; wp_reset_query() ?>
							</div>
						</div>
					</div>

				<?php endif; ?>
			</div>
		</div>
	</div>

<?php
get_footer();
