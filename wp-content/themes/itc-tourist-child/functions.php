<?php

function add_theme_scripts() {

/* CSS files */
  wp_enqueue_style( 'reset', get_stylesheet_directory_uri() . '/css/reset.css', array(), '1.1', 'all');

  wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css', array(), '1.1', 'all');

  //wp_enqueue_style( 'animate', get_stylesheet_directory_uri() . '/css/animate.css', array(), '1.1', 'all');

  wp_enqueue_style( 'mmenu-light', get_stylesheet_directory_uri() . '/css/mmenu-light.css', array(), '', 'all' );

  wp_enqueue_style( 'demo', get_stylesheet_directory_uri() . '/css/demo.css', array(), '', 'all' );

  wp_enqueue_style( 'owl-carousel', get_stylesheet_directory_uri() . '/css/owl-carousel.css', array(), '1.1', 'all');

  wp_enqueue_style( 'lightgallery', get_stylesheet_directory_uri() . '/css/lightgallery.css', array(), '1.1', 'all');

 //wp_enqueue_style( 'flexslider', get_stylesheet_directory_uri() . '/css/flexslider.css', array(), '1.1', 'all');

  wp_enqueue_style( 'Google-font', 'https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;600;700&display=swap', array(), '', 'all' );

  //wp_enqueue_style( 'examples-css', get_stylesheet_directory_uri() . '/css/examples.css', array(), '', 'all' );

  wp_enqueue_style( 'style', get_stylesheet_uri() );


  /* JS files */ 

  //wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js', array ( 'jquery' ), 1.1, true);

  wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', array ( 'jquery' ), 1.1, true);

  wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', array ( 'jquery' ), 1.1, true);

  wp_enqueue_script( 'mmenu-light', get_stylesheet_directory_uri() . '/js/mmenu-light.js', array('jquery'), '', true );

  wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/js/custom.js', array ( 'jquery' ), 1.1, true);

  //wp_enqueue_style( 'picturefill', 'https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js', array( 'jquery' ),  1.1, true );

  wp_enqueue_script( 'lightgallery', get_stylesheet_directory_uri() . '/js/lightgallery-all.min.js', array ( 'jquery' ), 1.1, true);

  wp_enqueue_script( 'mousewheel', get_stylesheet_directory_uri() . '/js/jquery.mousewheel.min.js', array ( 'jquery' ), 1.1, true);

  wp_enqueue_script( 'owl-carousel', get_stylesheet_directory_uri() . '/js/owl-carousel.js', array ( 'jquery' ), 1.1, true);

}

add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


/* admin css*/

add_action( 'admin_enqueue_scripts', 'load_admin_style' );

function load_admin_style() {

  wp_enqueue_style( 'admin_css', get_stylesheet_directory_uri() . '/css/admin-style.css', array(), '1.1', 'all' );

}

/* Remove parent theme script  */
function aspiria_remove_scripts() {
  wp_dequeue_script( 'twentynineteen-touch-navigation' );
}
add_action( 'wp_print_scripts', 'aspiria_remove_scripts', 100 );





add_theme_support( 'post-thumbnails' );
add_image_size( 'ponude-img', 390, 275, true );
add_image_size( 'vesti-img', 220, 140, true );
add_image_size( 'header-img', 1600 );
add_image_size( 'katalog-img', 350 );



/* Theme Options */

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Contact Settings',
		'menu_title'	=> 'Contact', 
		'parent_slug'	=> 'theme-general-settings',
	));

  acf_add_options_sub_page(array(
    'page_title'  => 'Theme Footer Settings',
    'menu_title'  => 'Footer',
    'parent_slug' => 'theme-general-settings',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Avio prevoznici',
    'menu_title'  => 'Avio prevoznici',
    'parent_slug' => 'theme-general-settings',
  ));



}



include( STYLESHEETPATH . '/inc/post-types/CPT.php'); 


/**
* Poslovi CPT
*/

include( STYLESHEETPATH . '/inc/post-types/register-poslovi.php');

/**
* Autobusi CPT
*/

include( STYLESHEETPATH . '/inc/post-types/register-autobusi.php');

/**
 * Avio karte
 */

include( STYLESHEETPATH . '/inc/post-types/register-aviokarte.php');

// /**
//  * Team Custom Post Type
//  */

// include( STYLESHEETPATH . '/inc/post-types/register-team.php');



function wpb_widgets_init() {
    register_sidebar( array(
        'name'          => 'Footer 1',
        'id'            => 'footer-1',
        'before_widget' => '<div class="chw-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="chw-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer 2',
        'id'            => 'footer-2',
        'before_widget' => '<div class="chw-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="chw-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer 3',
        'id'            => 'footer-3',
        'before_widget' => '<div class="chw-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="chw-title">',
        'after_title'   => '</h4>',
    ) );
}

add_action( 'widgets_init', 'wpb_widgets_init' );



add_action( 'wp' , function(){
  remove_filter( 'wp_nav_menu', 'twentynineteen_add_ellipses_to_nav', 10, 2 );
  remove_filter( 'walker_nav_menu_start_el', 'twentynineteen_add_dropdown_icons', 10, 4 );
  remove_filter( 'nav_menu_link_attributes', 'twentynineteen_nav_menu_link_attributes', 10, 4 );
  remove_filter( 'wp_nav_menu_objects', 'twentynineteen_add_mobile_parent_nav_menu_items', 10, 2 );
});






// Move Yoast to bottom
function yoasttobottom() {
  return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

/*remove yoast on cpt*/
function my_remove_wp_seo_meta_box() {
  remove_meta_box('wpseo_meta', 'aviokarte', 'normal');
}
add_action('add_meta_boxes', 'my_remove_wp_seo_meta_box', 100);



/* Popuni polaznike iz option field*/
function acf_load_avio_preoznici( $field ) {
    // reset choices
    $field['choices'] = array();
  // if has rows
    if( have_rows('prevoznici', 'option') ) {
        // while has rows
        while( have_rows('prevoznici', 'option') ) {
            // instantiate row
            the_row();
            // vars
            $value = get_sub_field('logo');
            $label = get_sub_field('naziv');
                  // append to choices
            $field['choices'][ $value ] = $label;
        }
  }
    // return the field
    return $field;
}
add_filter('acf/load_field/name=prevoznik', 'acf_load_avio_preoznici');


/* Popuni gradove polaska iz option field*/
function acf_load_polasci( $field ) {
    // reset choices
    $field['choices'] = array();
  // if has rows
    if( have_rows('destinacije', 'option') ) {
        // while has rows
        while( have_rows('destinacije', 'option') ) {
            // instantiate row
            the_row();
            // vars
            $value = get_sub_field('skracenica');
            $label = get_sub_field('grad');
                  // append to choices
            $field['choices'][ $value ] = $label;
        }
  }
    // return the field
    return $field;
}
add_filter('acf/load_field/name=polazak', 'acf_load_polasci');
add_filter('acf/load_field/name=dolazak', 'acf_load_polasci');





// function decode_entities_full($string, $quotes = ENT_COMPAT, $charset = 'ISO-8859-1') {
//     return html_entity_decode(preg_replace_callback('/&([a-zA-Z][a-zA-Z0-9]+);/', 'convert_entity', $string), $quotes, $charset); 
// }

// function convert_entity($matches, $destroy = true) {
//   static $table = array('quot' => '&#34;','amp' => '&#38;','lt' => '&#60;','gt' => '&#62;','OElig' => '&#338;','oelig' => '&#339;','Scaron' => '&#352;','scaron' => '&#353;','Yuml' => '&#376;','circ' => '&#710;','tilde' => '&#732;','ensp' => '&#8194;','emsp' => '&#8195;','thinsp' => '&#8201;','zwnj' => '&#8204;','zwj' => '&#8205;','lrm' => '&#8206;','rlm' => '&#8207;','ndash' => '&#8211;','mdash' => '&#8212;','lsquo' => '&#8216;','rsquo' => '&#8217;','sbquo' => '&#8218;','ldquo' => '&#8220;','rdquo' => '&#8221;','bdquo' => '&#8222;','dagger' => '&#8224;','Dagger' => '&#8225;','permil' => '&#8240;','lsaquo' => '&#8249;','rsaquo' => '&#8250;','euro' => '&#8364;','fnof' => '&#402;','Alpha' => '&#913;','Beta' => '&#914;','Gamma' => '&#915;','Delta' => '&#916;','Epsilon' => '&#917;','Zeta' => '&#918;','Eta' => '&#919;','Theta' => '&#920;','Iota' => '&#921;','Kappa' => '&#922;','Lambda' => '&#923;','Mu' => '&#924;','Nu' => '&#925;','Xi' => '&#926;','Omicron' => '&#927;','Pi' => '&#928;','Rho' => '&#929;','Sigma' => '&#931;','Tau' => '&#932;','Upsilon' => '&#933;','Phi' => '&#934;','Chi' => '&#935;','Psi' => '&#936;','Omega' => '&#937;','alpha' => '&#945;','beta' => '&#946;','gamma' => '&#947;','delta' => '&#948;','epsilon' => '&#949;','zeta' => '&#950;','eta' => '&#951;','theta' => '&#952;','iota' => '&#953;','kappa' => '&#954;','lambda' => '&#955;','mu' => '&#956;','nu' => '&#957;','xi' => '&#958;','omicron' => '&#959;','pi' => '&#960;','rho' => '&#961;','sigmaf' => '&#962;','sigma' => '&#963;','tau' => '&#964;','upsilon' => '&#965;','phi' => '&#966;','chi' => '&#967;','psi' => '&#968;','omega' => '&#969;','thetasym' => '&#977;','upsih' => '&#978;','piv' => '&#982;','bull' => '&#8226;','hellip' => '&#8230;','prime' => '&#8242;','Prime' => '&#8243;','oline' => '&#8254;','frasl' => '&#8260;','weierp' => '&#8472;','image' => '&#8465;','real' => '&#8476;','trade' => '&#8482;','alefsym' => '&#8501;','larr' => '&#8592;','uarr' => '&#8593;','rarr' => '&#8594;','darr' => '&#8595;','harr' => '&#8596;','crarr' => '&#8629;','lArr' => '&#8656;','uArr' => '&#8657;','rArr' => '&#8658;','dArr' => '&#8659;','hArr' => '&#8660;','forall' => '&#8704;','part' => '&#8706;','exist' => '&#8707;','empty' => '&#8709;','nabla' => '&#8711;','isin' => '&#8712;','notin' => '&#8713;','ni' => '&#8715;','prod' => '&#8719;','sum' => '&#8721;','minus' => '&#8722;','lowast' => '&#8727;','radic' => '&#8730;','prop' => '&#8733;','infin' => '&#8734;','ang' => '&#8736;','and' => '&#8743;','or' => '&#8744;','cap' => '&#8745;','cup' => '&#8746;','int' => '&#8747;','there4' => '&#8756;','sim' => '&#8764;','cong' => '&#8773;','asymp' => '&#8776;','ne' => '&#8800;','equiv' => '&#8801;','le' => '&#8804;','ge' => '&#8805;','sub' => '&#8834;','sup' => '&#8835;','nsub' => '&#8836;','sube' => '&#8838;','supe' => '&#8839;','oplus' => '&#8853;','otimes' => '&#8855;','perp' => '&#8869;','sdot' => '&#8901;','lceil' => '&#8968;','rceil' => '&#8969;','lfloor' => '&#8970;','rfloor' => '&#8971;','lang' => '&#9001;','rang' => '&#9002;','loz' => '&#9674;','spades' => '&#9824;','clubs' => '&#9827;','hearts' => '&#9829;','diams' => '&#9830;','nbsp' => '&#160;','iexcl' => '&#161;','cent' => '&#162;','pound' => '&#163;','curren' => '&#164;','yen' => '&#165;','brvbar' => '&#166;','sect' => '&#167;','uml' => '&#168;','copy' => '&#169;','ordf' => '&#170;','laquo' => '&#171;','not' => '&#172;','shy' => '&#173;','reg' => '&#174;','macr' => '&#175;','deg' => '&#176;','plusmn' => '&#177;','sup2' => '&#178;','sup3' => '&#179;','acute' => '&#180;','micro' => '&#181;','para' => '&#182;','middot' => '&#183;','cedil' => '&#184;','sup1' => '&#185;','ordm' => '&#186;','raquo' => '&#187;','frac14' => '&#188;','frac12' => '&#189;','frac34' => '&#190;','iquest' => '&#191;','Agrave' => '&#192;','Aacute' => '&#193;','Acirc' => '&#194;','Atilde' => '&#195;','Auml' => '&#196;','Aring' => '&#197;','AElig' => '&#198;','Ccedil' => '&#199;','Egrave' => '&#200;','Eacute' => '&#201;','Ecirc' => '&#202;','Euml' => '&#203;','Igrave' => '&#204;','Iacute' => '&#205;','Icirc' => '&#206;','Iuml' => '&#207;','ETH' => '&#208;','Ntilde' => '&#209;','Ograve' => '&#210;','Oacute' => '&#211;','Ocirc' => '&#212;','Otilde' => '&#213;','Ouml' => '&#214;','times' => '&#215;','Oslash' => '&#216;','Ugrave' => '&#217;','Uacute' => '&#218;','Ucirc' => '&#219;','Uuml' => '&#220;','Yacute' => '&#221;','THORN' => '&#222;','szlig' => '&#223;','agrave' => '&#224;','aacute' => '&#225;','acirc' => '&#226;','atilde' => '&#227;','auml' => '&#228;','aring' => '&#229;','aelig' => '&#230;','ccedil' => '&#231;','egrave' => '&#232;','eacute' => '&#233;','ecirc' => '&#234;','euml' => '&#235;','igrave' => '&#236;','iacute' => '&#237;','icirc' => '&#238;','iuml' => '&#239;','eth' => '&#240;','ntilde' => '&#241;','ograve' => '&#242;','oacute' => '&#243;','ocirc' => '&#244;','otilde' => '&#245;','ouml' => '&#246;','divide' => '&#247;','oslash' => '&#248;','ugrave' => '&#249;','uacute' => '&#250;','ucirc' => '&#251;','uuml' => '&#252;','yacute' => '&#253;','thorn' => '&#254;','yuml' => '&#255;'
//                        );
//   if (isset($table[$matches[1]])) return $table[$matches[1]];
//   // else 
//   return $destroy ? '' : $matches[0];
// }