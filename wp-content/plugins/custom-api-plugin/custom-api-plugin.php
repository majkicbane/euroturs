<?php
/**
 * @package CustomEurotursAPIPlugin
 */
/*
Plugin Name: Custom Euroturs API Plugin 2020
Description: 
Version: 1.0.0
Author: ITCentar
License: GPLv2 or later
Text Domain: custom-api-plugin
*/

// If this file is called firectly, abort!!!
defined( 'ABSPATH' ) or die( 'Access is denied!' );

// Require once the Composer Autoload
if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

/**
 * The code that runs during plugin activation
 */
function activate_custom_plugin() {
    ApiPlugin\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_custom_plugin' );

/**
 * The code that runs during plugin deactivation
 */
function deactivate_custom_plugin() {
    ApiPlugin\Base\Deactivate::deactivate();
}
register_deactivation_hook( __FILE__, 'deactivate_custom_plugin' );

// /**
//  * Initialize all the core classes of the plugin
//  */
if ( class_exists( 'ApiPlugin\\Init' ) ) {
	ApiPlugin\Init::register_services();
}