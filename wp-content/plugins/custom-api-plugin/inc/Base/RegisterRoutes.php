<?php
/**
 * @package CustomEurotursAPIPlugin
 */
namespace ApiPlugin\Base;

class RegisterRoutes
{
    private $_rest_route_namespace = 'api/v1';

    public function register() 
    {
		add_action( 'rest_api_init', array( $this, 'rest_api_init' ) );
    }

	public function rest_api_init() {

		register_rest_route( $this->_rest_route_namespace, '/test', array(
			'methods'  => 'GET',
            'callback' => array( 'ApiPlugin\Controllers\ContentController', 'test' ),
        ) );
        
        register_rest_route( $this->_rest_route_namespace, '/pull', array(
			'methods'  => 'GET',
            'callback' => array( 'ApiPlugin\Controllers\ContentController', 'pull' ),
        ) );
        
        register_rest_route( $this->_rest_route_namespace, '/pull-single', array(
			'methods'  => 'GET',
            'callback' => array( 'ApiPlugin\Controllers\ContentController', 'pullSingle' ),
        ) );
        
        register_rest_route( $this->_rest_route_namespace, '/pull-descriptors', array(
			'methods'  => 'GET',
            'callback' => array( 'ApiPlugin\Controllers\ContentController', 'pullDescriptors' ),
		) );

    }

}