<?php
/**
 * @package CustomEurotursAPIPlugin
 */

namespace ApiPlugin\Base;

use ApiPlugin\Controllers\AccomodationController;

class RegisterShortcodes
{
    public function register() 
    {
        //rates
		add_shortcode( 'accomodation-rates', [ new AccomodationController, 'showRates' ] );

	}
}