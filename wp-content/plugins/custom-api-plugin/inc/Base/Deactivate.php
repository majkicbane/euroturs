<?php
/**
 * @package CustomEurotursAPIPlugin
 */
namespace ApiPlugin\Base;

class Deactivate
{
	public static function deactivate() {
		flush_rewrite_rules();
	}
}