<?php
/**
 * @package CustomEurotursAPIPlugin
 */
namespace ApiPlugin\Base;

class Activate
{
	public static function activate() {
		flush_rewrite_rules();
	}
}