<?php
/**
 * @package CustomEurotursAPIPlugin
 */

namespace ApiPlugin\Controllers;

class PackageController extends Controller
{
    CONST CATEGORIES = [
        'LETOVANJE'   => 'leto',
        'ZIMOVANJE'   => 'zima',
        'KRUŽNA TURA' => 'kruzna-tura',
        'WELLNESS'    => 'wellnes'
    ];

    public static function get( $ref = '', $extras = false )
    {
        $extras = $extras ? '?extended=true&attributes=true' : '';
        $url = self::URL."packages/$ref".$extras;
        $response = self::contactApi($url);
        $result = json_decode($response);
        $packCategory = $result->category;

        $data['category'] = in_array( $packCategory, array_flip( self::CATEGORIES ) ) ?  self::CATEGORIES[$packCategory] : 'putovanja';
        $descriptors = $result->descriptors;

        if( empty($descriptors) ){
            return false;
        }
        
        $data['country'] = $descriptors[0];
        $data['region'] = $descriptors[1];
        $data['city'] = $descriptors[2];
        $accomodations = [];
        $rooms = [];
        foreach($descriptors as $descriptor){
            if( $descriptor->type == 'accomodation' ){
                $accomodations[] = $descriptor;
            }
            if( $descriptor->type == 'room' ){
                $rooms[$descriptor->parentDescriptorKey][] = $descriptor;
            }
        }
        foreach($accomodations as $accomodation){
            $accomodation->rooms = $rooms[$accomodation->descriptorKey];
            $validRuns = [];
            foreach($result->runs as $run){
                if($run->removed !== true){
                    $validRuns[] = $run;
                }
            }
            $accomodation->runs = $validRuns;
            $accomodationRuns = [];

            foreach($accomodation->rooms as $room){
                $services = [];
                $rates = [];
                $runs = [];
                
                
                foreach( $result->services as $service ){

                    if( strpos($service->serviceRef, $accomodation->accomodationRef.'.'.$room->roomRef) !== false ){
                        $rates = [];
                        foreach( $result->rates as $rate ){
                            if($service->serviceKey == $rate->serviceKey){
                                
                                foreach($result->runs as $run){

                                    if($run->removed !== true){

                                        if(in_array($run->seasonCode, $rate->seasonCode)){
                                            $add = true;
                                            $rates[] = $rate;
                                            $accomodationRuns[] = $run;
                                        }
                                        
                                    }

                                }
                            }
                        }
    
                        if($add === true){
                            $services[] = [
                                'service' => $service,
                                'rates'   => $rates
                            ];
                        }
                        
                    }
                }

                $runs['services'] = $services;
                $room->runs = $runs;
            }

            $runs = [];
            foreach($accomodation->runs as $run){
                if(in_array($run, $accomodationRuns)){
                    $runs[] = $run;
                }
            }
            $accomodation->runs = $runs;
        }

        $data['accomodations'] = $accomodations;
        $data['accomodation_type'] = $descriptors[0]->locationRef == 84 ? sanitize_title_with_dashes($accomodations[0]->accomodationType) : false;
        
        return $data;
    }

    public static function createPage( $location, $parentId = 0 )
    {
        // $location = $descriptor->descriptors[0];

        $myPost = [
            'post_title'    => wp_strip_all_tags( $location->name ),
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_parent'   => $parentId,
            'post_type'     => 'page',
            'post_content'  => $location->description,
            'meta_input'    => [
                '_wp_page_template' => 'templates/template-ponude.php',
                'location_ref'      => $location->locationRef
            ],
        ];

        $exists = self::exists( $location->locationRef, 'location' );
        if( $exists ){
            //update
            $myPost['ID'] = $exists;
            wp_update_post( $myPost );
            return $exists;
        }else{
            //insert
            $postId = wp_insert_post( $myPost );
            return $postId;
        }
    }

}