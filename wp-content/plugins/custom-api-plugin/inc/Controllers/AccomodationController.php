<?php
/**
 * @package CustomEurotursAPIPlugin
 */

namespace ApiPlugin\Controllers;

class AccomodationController extends Controller
{
    CONST DISTANCES = [
        'gondola_distance' => 'Udaljenost od gondole',
        'skibus_distance'  => 'Udaljenost od gondole',
        'beach_distance'   => 'Udaljenost od plaže',
        'center_distance'  => 'Udaljenost od centra',
        'airport_distance' => 'Udaljenost od aerodroma',
    ];

    public static function get( $ref, $parent = '' )
    {
        $url = self::URL."descriptors$parent/$ref?extended=true&attributes=true";
        $response = self::contactApi($url);
        $data = json_decode($response);

        $accomodation = $data->descriptors[3];
        $rooms = [];
        foreach($data->descriptors as $descriptor){
            if( $descriptor->type == 'room' ){
                $rooms[] = $descriptor;
            }
        }

        $accomodationType = $descriptors[0]->locationRef == 84 ? sanitize_title_with_dashes($accomodation->accomodationType) : false;

        return ['accomodation' => $accomodation, 'accomodationType' => $accomodationType, 'rooms' => $rooms];
    }

    public static function createPage( $accomodation, $parentId = 0, $packRef = 0 )
    {
        $postTitle = $accomodation->name;
        $type = $accomodation->accomodationType != 'Apartmani' ? $accomodation->accomodationType : 'Apartman / Studio';

        $metaData = [
            'packRef' => $packRef,
            '_wp_page_template'  => 'templates/template-vile.php',
            'kategorija'         => $accomodation->categoryName,
            'accomodation_type'  => $type,
            'accomodation_ref'   => $accomodation->accomodationRef,
            'latitude'           => $accomodation->latitude,
            'longitude'          => $accomodation->longitude,
            'phone'              => $accomodation->phone,
            'website'            => $accomodation->website,
            'rooms'              => count($accomodation->rooms),
            'runs'               => $accomodation->runs
        ];

        $counter = 0;
        $roomsData = [];
        foreach($accomodation->rooms as $room){
            $singleRoomData = self::addRoom( $room, $counter );
            $roomsData += $singleRoomData;
            $counter++;
        }
        $metaData += $roomsData;

        $attrs = self::getAttrs( $accomodation->attrs );     

        $metaData['features'] = $attrs['features'];
        $metaData['activity'] = $attrs['activities'];
        $metaData += $attrs['others'];

        $myPost = [
            'post_title'     => wp_strip_all_tags( $postTitle ),
            'post_content'   => $accomodation->description,
            'post_parent'    => $parentId,
            'post_status'    => 'publish',
            'post_author'    => 1,
            'post_type'      => 'page',
            'comment_status' => 'open',
            'meta_input'     => $metaData
        ];

        $parentId = $parentId != 0 ? $parentId : false;
        $exists = self::exists( $accomodation->accomodationRef, 'accomodation', $parentId );
        if( $exists ){
            //update
            $postId = $exists;
            $update = $myPost['ID'] = $exists;
            $update = wp_update_post( $myPost );

            return 'pID: '.$parentId.' updated, post title: '.get_post($postId)->post_title.' - post_parent: '.get_post($postId)->post_parent;
        }else{
            //insert
            $postId = wp_insert_post( $myPost );

            $gallery = [];
            foreach($accomodation->media as $media){
                if( $media->mediaType == "IMAGE" ){
                    
                    $originalSize = ( ! empty($media->flags) ) || empty($gallery);

                    $imageInfo = ['id' => $media->mediaRef, 'name' => $media->name, 'description' => $media->description];
                    $attach_id = self::saveImage( $imageInfo, $postId, $originalSize );

                    if( $originalSize ){
                        set_post_thumbnail( $postId, $attach_id );
                    }

                    update_post_meta( $attach_id, 'media_ref', $media->mediaRef );
                    $gallery[] = (string)$attach_id;
                }
            }

            update_post_meta($postId, 'galerija', $gallery);
            return 'inserted';
        }
    }

    public static function getAttrs( $attrs )
    {
        $features = $activities = $others = [];
        
        foreach( $attrs as $attr ){
            if($attr->group == 'general'){
                $features[] = ucfirst($attr->key);
            }

            if($attr->group == 'activity'){
                $activities[] = ucfirst($attr->key);
            }
            
            if($attr->group == 'location'){
                if(in_array($attr->key, self::DISTANCES)){
                    $name = array_flip(self::DISTANCES)[$attr->key];
                    if(stripos($attr->value, 'km')){
                        $value = (int)(str_replace( 'km', '', $attr->value))*1000;
                    }else{
                        $value = (int)(str_replace( 'm', '', $attr->value));
                    }
                    $others[$name] = $value;
                }
            }
        }
        return ['features' => $features, 'activities' => $activities, 'others' => $others ];
    }

    public static function addRoom( $room, $counter ){
        $meta = [];

        $roomFeatures = [];
        foreach($room->attrs as $feature){
            $roomFeatures[] = ucfirst($feature->key);
        }

        $meta["rooms_".$counter."_room_name"] = $room->name;
        $meta["rooms_".$counter."_room_description"] = $room->description;
        $meta["rooms_".$counter."_room_features"] = $roomFeatures;
        $meta["rooms_".$counter."_min_adults"] = $room->capacity0;
        $meta["rooms_".$counter."_additional_capacity_kids"] = $room->capacity1;
        $meta["rooms_".$counter."_additional_capacity_beds"] = $room->capacity2;
        $meta["rooms_".$counter."_runs"] = $room->runs;

        return $meta;
    }

    public static function showRates( $attrs )
    {
        $attrs = shortcode_atts( [
            'hotel'  => false
        ], $attrs );
        $hotelId = $attrs['hotel'];

        if( $hotelId === false ){
            return 'Hotel id not set';
        }

        $hotel = get_post($hotelId);

        if( $hotel === null ){
            return "Hotel with given id doesn't exist, is null";
        }

        if( $hotel->post_type != 'page' ){
            return "Hotel with given id doesn't exist, is not a page";
        }

        if( $hotel->accomodation_type == '' ){
            return "Hotel with given id doesn't exist, accomodation_type is empty";
        }

        $dates = $days = $rates = '';
        foreach($hotel->runs as $run){

            if(isset($run->duration)){
                $begin = date( 'd. m.', strtotime($run->date) );
                $end = date( 'd. m.', strtotime($run->date. ' + '.$run->duration.' days') );
                $duration = $run->duration.'/'.($run->duration - 1);
            }else{
                $start = new \DateTime($run->begin);
                $end = new \DateTime($run->end);
                $duration = $start->diff($end)->days.'/'.($start->diff($end)->days - 1);

                $begin = date( 'd. m.', strtotime($run->begin) );
                $end = date( 'd. m.', strtotime($run->end) );
            }

            $dates .= "
            <th class='text-center'>
                $begin<br>$end
            </th>";

            $days .= "
            <td>$duration</td>";
        }

        $rooms = '';
        for($i = 0; $i < $hotel->rooms; $i++){
            $attr = "rooms_".$i."_room_name";
            $name = $hotel->$attr;

            $rooms .= "
            <tr class='l-blue-cell'>
                <td colspan='100%' class='text-center'>$name</td>
            </tr>";

            $attr = "rooms_".$i."_runs";
            $runs = $hotel->$attr;
            $services = $runs['services'];

            foreach($services as $service){
                $additionals = [];
                $boarding = $service['service']->boarding;
                $serviceUnit = $service['service']->serviceUnit;
                $baseRates = '';
                
                foreach($service['rates'] as $rate){
                    $currency = $rate->currency;
                    $currency = str_replace("EUR", "€", $currency);
                    $baseRates .= "<td class='bold-cell'><b>$rate->ammount $currency</b></td>";
                    if($rate->corrections !== []){
                        foreach($rate->corrections as $additional){
                            if($additional->type == 'child' || $additional->type == 'infant'){
                                $common = $additional->common === true ? '(u zajedničkom ležaju)' : '';
                                $key = "$additional->childIndex. DETE $common ($additional->ageFrom-$additional->ageTo)";
                                // $key = $additional->childIndex.'. DETE '.$common.' ('.$additional->ageFrom.'-'.$additional->ageTo.')';
                            }else{
                                $key = $additional->descriptions[0];
                                $key = str_replace( "EXTRA BED", "DODATNI KREVET", $key );
                                // $keyName = str_replace( "CHILD", "DETE", $key );
                            }
                            $value = $additional->ammount;
                            $additionals[$key][] = $value." ".$currency;
                        }
                    }
                }

                $add = '';
                foreach($additionals as $key => $value){            

                    $add .= "
                    <tr>
                    <td>$key</td>";
                    
                    foreach($value as $amount){
                        $add .= "<td>$amount</td>";
                    }
                    $add .= "</tr>";
                }
    
                $serviceUnit = $serviceUnit == "PER_PERSON" ? "PO OSOBI" : $serviceUnit;
                $rowspan = count($additionals)+1;
                $rooms .= "
                <tr>
                    <td rowspan=$rowspan class='boarding-cell text-center bold-cell' style='vertical-align:middle;'><b>$boarding</b></td>
                    <td>$serviceUnit</td>
                    $baseRates
                    $add
                </tr>";
            }
            
        }

        $html = "
        <h3>package: $hotel->packRef</h3>
        <h3>accomodation: $hotel->accomodation_ref</h3>
        <h3>post: $hotel->ID</h3>
        <table>
            <tr class='d-blue-cell'>
                <th class='text-left' colspan=2>
                    Polazak<br>Povratak
                </th>
                $dates
            </tr>
            <tr class='gold-cell'>
                <td class='text-left' colspan=2>Dana/Noći</td>
                $days
            </tr>
            $rates
            $rooms
        </table>";

        return $html;
    }

}