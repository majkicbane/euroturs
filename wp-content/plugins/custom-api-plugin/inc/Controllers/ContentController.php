<?php
/**
 * @package CustomEurotursAPIPlugin
 */

namespace ApiPlugin\Controllers;

class ContentController extends Controller
{
    public static function pullSingle()
    {
        $packageId = 5495;
        $data = PackageController::get( $packageId, 1 );
        ContentController::addSingle( $data, $packageId );
    }

    public static function pull()
    {
        $url = self::URL."packages";
        $packages = json_decode( self::contactApi($url) );
        $packCounter = 0;
        $descCounter = 0;
        $counter = 0;

        foreach($packages->packs as $package){
            $data = PackageController::get( $package->packRef, 1 );
            if( ! is_bool($data) ){
                if( ! ($data['accomodation_type'] == 'prevoz') ){
                    $counter++;
                    $descCounter += ContentController::addSingle( $data, $package->packRef );
                }
            }
            $packCounter++;
        }
        return 'all: '.$packCounter.' , saved: '.$counter.' , desc counter: '. $descCounter;
    }

    public static function pullDescriptors()
    {
        $url = self::URL."descriptors";
        $descriptors = json_decode( self::contactApi($url) );
        // return $descriptors;

        $countries = [];
        $hotelsFull = [];
        $testCountries = [ 84, 196, 34 ];
        $testCountries = [ 84 ];
        // foreach($descriptors->descriptors as $country){
        foreach($testCountries as $country){
            $countryInfo = LocationController::get( $country );
            // $countryPostId = LocationController::createPage($countryInfo['main']);
            $countryId = $countryInfo['main']->locationRef;
            $fullCountries[] = $countryInfo;
            $regions = $countryInfo['array'];

            foreach($regions as $region){
                $regionId = $region->locationRef;
                $regionInfo = LocationController::get( $regionId, "/$countryId", 1 );
                $cities = $regionInfo['array'];

                foreach($cities as $city){
                    $cityId = $city->locationRef;
                    $cityInfo = LocationController::get( $cityId, "/$countryId/$regionId", 2 );
                    $hotels = $cityInfo['array'];
                    foreach($hotels as $hotel){
                        $hotelId = $hotel->accomodationRef;
                        $hotelsFull[$city->name][] = AccomodationController::get( $hotelId, "/$countryId/$regionId/$cityId", 2 );
                    }
                }
            }
        }
        // return $regions;
        return $hotelsFull;
        return $hotels;
        return $cities;
        return $fullCountries[0]['array'];
        return $countries;
    }

    public static function addSingle( $data, $packRef )
    {
        $counter = 0;
        $basePostId = CategoryController::createPage( ucfirst($data['category']) );
        $countryPostId = LocationController::createPage( $data['country'], $basePostId );

        if($data['country']->locationRef == 84 || $data['country']->locationRef == 225){
            if($data['accomodation_type'] == false){
                $counter++;
                $regionPostId = LocationController::createPage( $data['region'], $countryPostId );
            }else{
                $counter += 2;
                $accomodationTypePostId = CategoryController::createPage( $data['accomodation_type'], $countryPostId );
                $regionPostId = LocationController::createPage( $data['region'], $accomodationTypePostId );
            }
        }else{
            $regionPostId = $countryPostId;
        }

        $cityPostId = LocationController::createPage( $data['city'], $regionPostId );
        $counter++;
    
        $accomodations = $data['accomodations'];
        foreach( $accomodations as $accomodation ){
            if( $accomodation->accomodationType != 'Prevoz' ){
                $counter++;
                AccomodationController::createPage( $accomodation, $cityPostId, $packRef );
            }
        }
        return $counter;
    }

    public static function test()
    {
        // return get_field_object('field_5fa3fc174906e');
        // return get_field_object('longitude');
        // return print_r(get_field('longitude', false, false));
        // return get_field_object('longitude', 1078);
        // $url = self::URL."descriptors/84/514/131/8007?extended=true&attributes=true";
        // $url = self::URL."descriptors/84/715/722/2391?extended=true&attributes=true";
        // $url = self::URL."packages/5022?extended=true&attributes=true";
        // $url = self::URL."packages/4964?extended=true&attributes=true";
        $url = self::URL."packages/5728?extended=true&attributes=true";
        $url = self::URL."packages/5637?extended=true&attributes=true"; //vise rates nego sto treba u cenovniku
        $url = self::URL."packages/5718?extended=true&attributes=true";
        //5495 sa puno hotela i puno runs koji ne pripadaju svim hotelima
        $url = self::URL."packages/5495?extended=true&attributes=true";
        $url = self::URL."packages/5709?extended=true&attributes=true";
        //1799 sa cenama 0
        // $url = self::URL."packages/1799?extended=true&attributes=true";
        // $url = self::URL."packages/5676?extended=true&attributes=true";
        // $url = self::URL."packages/5603?extended=true&attributes=true";
        // $url = self::URL."packages/5601";
        // $url = self::URL."packages/5020";
        // $url = self::URL."descriptors/84/557/628/8022?extended=true&attributes=true";
        // $url = self::URL."descriptors/234/1328/2861/";

        
        // return PackageController::get( 1799, 1 );
        // return PackageController::get( 5604, 1 );
        // return PackageController::get( 5495, 1 );

        // return PackageController::get( 4964, 1 )['accomodations'][0]->rooms[0];
        
        $desc = json_decode( self::contactApi($url) );
        return $desc;

        // $hotel = get_post(4781);
        $hotel = get_post(3072);
        // return $hotel->packRef;
        $attr = "rooms_0_runs";
        $runs = $hotel->$attr;
        return $runs['services'][0]['rates'];


        $print = '';
        foreach($runs['services'] as $service){
            $boarding = $service['service']->boarding;
            $print .= "
            <tr class='l-blue-cell'>
                <td colspan='100%' class='text-center'>$boarding</td>
            </tr>";

            $serviceUnit = $service['service']->serviceUnit;

            $baseRates = '';
            foreach($service['rates'] as $rate){
                $baseRates .= "<td>$rate->ammount</td>";
            }

            $print .= "
            <tr>
                <td>$serviceUnit</td>
                <td>15</td>
                $baseRates
            </tr>";
            
        }
        // return $print;
        // return $runs['services'][1]['rates'];
        return $runs['services'];

        $accomodation = get_post(1042);
        $meta = get_post_meta($accomodation->ID);
        return $meta;
        $rows = get_fields( $accomodation->ID );
        return $rows;
        if( $rows ) {
            echo '<ul class="slides">';
            foreach( $rows as $row ) {
                $image = $row['image'];
                echo '<li>';
                    echo wp_get_attachment_image( $image, 'full' );
                    echo wpautop( $row['caption'] );
                echo '</li>';
            }
        }
        echo '</ul>';
        // return $accomodation;
    }
}