<?php
/**
 * @package CustomEurotursAPIPlugin
 */

namespace ApiPlugin\Controllers;

class Controller
{
    CONST USERNAME = 'itcentar@euroturs';
    CONST PASSWORD = '19cVid';
    CONST URL      = 'https://euroturs.turistklubnt.rs/api/v1/agents/96/';
    CONST IMG_URL  = 'https://euroturs.turistklubnt.rs/photo?photoId=';

    public static function contactApi( $url )
    {
        $ch = curl_init($url);
        
        curl_setopt($ch, CURLOPT_USERPWD, self::USERNAME . ":" . self::PASSWORD);  
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $response = curl_exec($ch);

        if(curl_errno($ch)){
            throw new Exception(curl_error($ch));
        }

        return $response;
    }

    public static function saveImage( $image, $parentPostId = null, $original = false )
    {
        $imageInfo = [
            'id' => $image['id'],
            'name' => $image['name'],
            'content' => $image['description']
        ];
        // Get the path to the upload directory.
        $wp_upload_dir = wp_upload_dir();

        $extra = $original ? "&size=-1" : '';
        
        $imageContent = file_get_contents( self::IMG_URL . $image['id'] . $extra );
        $imagePath = $wp_upload_dir['path'];
        // $filename should be the path to a file in the upload directory.
        $filename = $wp_upload_dir['path'].'/'.$image['name'];
        
        // Check the type of file. We'll use this as the 'post_mime_type'.
        $filetype = wp_check_filetype( basename( $filename ), null );
        
        file_put_contents( $imagePath.'/'.$image['name'], $imageContent );

        // Prepare an array of post data for the attachment.
        $attachment = array(
            'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
            'post_mime_type' => $filetype['type'],
            'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
            'post_content'   => $image['description'],
            'post_status'    => 'inherit'
        );
        
        // Insert the attachment.
        $attach_id = wp_insert_attachment( $attachment, $filename, $parentPostId );
        
        // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        
        // Generate the metadata for the attachment, and update the database record.
        $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
        wp_update_attachment_metadata( $attach_id, $attach_data );
        $description = $image['description'] != '' ? $image['description'] : preg_replace( '/\.[^.]+$/', '', basename( $filename ) );
        update_post_meta( $attach_id, '_wp_attachment_image_alt', $description);

        return $attach_id;
    }

    public static function exists( $ref, $type, $parentId = false )
    {
        $args = [
            'post_type'  => 'page',
            'meta_query' => [
                [
                    'key' => $type."_ref",
                    'value' => $ref,
                    'compare' => '=',
                ]
            ]
        ];
        if($parentId){
            $args['post_parent'] = $parentId;
        }
        $posts_all = get_posts( $args );

        return empty($posts_all) ? false : $posts_all[0]->ID;
    }
}