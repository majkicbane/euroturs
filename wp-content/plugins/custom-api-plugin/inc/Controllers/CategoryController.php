<?php
/**
 * @package CustomEurotursAPIPlugin
 */

namespace ApiPlugin\Controllers;

class CategoryController extends Controller
{
    public static function createPage( $category, $parentId = 0 )
    {
        $postName = sanitize_title_with_dashes($category);
        $postName = $postName == 'apartman-studio' ? 'grcka-apartmani' : $postName;

        $myPost = [
            'post_title'  => wp_strip_all_tags( $category ),
            'post_name'   => $postName,
            'post_status' => 'publish',
            'post_author' => 1,
            'post_parent' => $parentId,
            'post_type'   => 'page',
            'meta_input'  => [
                '_wp_page_template' => 'templates/template-ponude.php'
            ],
        ];

        $parentId = $parentId != 0 ? $parentId : false;
        $exists = self::existsCategory( $postName, $parentId );
        
        if( $exists ){
            //update
            $myPost['ID'] = $exists;
            wp_update_post( $myPost );
            return $exists;
        }else{
            //insert
            $postId = wp_insert_post( $myPost );
            return $postId;
        }
    }

    public static function existsCategory( $postName, $parentId = false )
    {
        $args = [
            'post_type' => 'page',
            'name' => $postName
        ];
        if($parentId){
            $args['post_parent'] = $parentId;
        }
        $posts_all = get_posts( $args );

        return empty($posts_all) ? false : $posts_all[0]->ID;
    }

}