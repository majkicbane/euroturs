<?php
/**
 * @package CustomEurotursAPIPlugin
 */

namespace ApiPlugin\Controllers;

class LocationController extends Controller
{
    public static function get( $ref, $parent = '', $depth = 0 )
    {
        $url = self::URL."descriptors$parent/$ref?extended=true&attributes=true";
        
        $response = self::contactApi($url);

        $descriptor = json_decode($response);

        $main = $descriptor->descriptors[$depth];
        $array = [];
        $counter = $depth == 2 ? -1: 0;
        foreach($descriptor->descriptors as $location){
            if( $counter > 1 && count($array) < 10 ){
                $array[] = $location;
            }

            $counter++;
        }

        return ['main' => $main, 'array' => $array];
    }

    public static function createPage( $location, $parentId = 0 )
    {
        $myPost = [
            'post_title'   => wp_strip_all_tags( $location->name ),
            'post_status'  => 'publish',
            'post_author'  => 1,
            'post_parent'  => $parentId,
            'post_type'    => 'page',
            'post_content' => $location->description,
            'meta_input'   => [
                '_wp_page_template' => 'templates/template-ponude.php',
                'location_ref'      => $location->locationRef,
                'location_type'     => $location->locationType
            ],
        ];

        $parentId = $parentId != 0 ? $parentId : false;
        $exists = self::exists( $location->locationRef, 'location', $parentId );
        if( $exists ){
            //update
            $myPost['ID'] = $exists;
            wp_update_post( $myPost );
            return $exists;
        }else{
            //insert
            $postId = wp_insert_post( $myPost );
            return $postId;
        }
    }
}